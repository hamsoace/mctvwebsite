# MyMoviesAfrica Website

![MyMoviesAfrica logo](./src/images/mma-logo.png)

### Site Modules

- User Registration and Login
- Landing page
- Search
- Listings
    - Favourites
    - Watched
    - Free
    - Paid
    - Shared content
- Tagging
    - Genres
    - Actors
    - Categories
    - Formats
- Media Playback
    - VideoJS
    - Playback from last point
    - Autoplay
    - Video suggestions
    - Cinema View
- Subscriptions
    - Track Usage
    - Invoicing
    - Payment confirmation
- User Accounts
    - Updating details
    - Reseting accounts
- Geo-gating
- AMP Support
- Parental Control


### Login and Authentication

This will be managed using Firebase. Authentication will comprise of:

- Login with Google
- Login with Facebook
- Login with Email/Password


### Content Streaming & Decryption

Content will be encoded and encrypted into MPEG-DASH format which will be streamed from our CDN.


### Secure `.env`

This is achieved by making config file readable and writable only by the user that created it:
`chmod 600 .env`


### Libraries Used

- ReactJS: Using [create-react-app](https://github.com/facebookincubator/create-react-app)
- ExpressPlay: Management of playback licenses 
- Firebase: User Authentication
- Google Analytics: Collect data on site usage
- Shaka Player (IE11+)
- `videojs-contrib-dash`
- [Video Thumbnails](https://www.npmjs.com/package/videothumbhtml5)
- [Video Hotkeys](https://github.com/ctd1500/videojs-hotkeys)
- [Browser Update](https://browser-update.org/)
- [Facebook Comments Plugin](https://developers.facebook.com/docs/plugins/comments)


### Search Engine Optimization

- [Open Graph](http://ogp.me/) - Creating rich objects
- [Open Search](http://www.opensearch.org/Documentation/Developer_how_to_guide) - Allows search from URL bar in Chrome
- [Generating Rich Snippets](https://builtvisible.com/micro-data-schema-org-guide-generating-rich-snippets/)
- Accelerated Media Pages (AMP) - [Examples](https://ampbyexample.com/samples_templates/product_page/)


# CONTENT ENCODING & PLAYBACK

### HLS Encoding Profiles
From this [post](https://github.com/videojs/videojs-contrib-hls/issues/653#issuecomment-213802425) on Github,

```
You're right, I get the same thing when disabling flash. That generally means something wrong with the encoding itself (flash is very forgiving), what did you use to encode the original video and how did you then segment it?

Try playing around with different profiles and sizes, start at baseline 3.0 and step up from there.

This is what I use, which works with the setup described above:

ffmpeg -y -async 1 -vsync -1 -analyzeduration 999999999 -i [INPUT] -movflags faststart -keyint_min 30 -x264opts "keyint=30:min-keyint=30:no-scenecut" -g 30 -filter:v "scale=iw*min(1280/iw\,720/ih):ih*min(1280/iw\,720/ih), pad=1280:720:(1280-iw*min(1280/iw\,720/ih))/2:(720-ih*min(1280/iw\,720/ih))/2" -r:v 30 -s 1280x720 -b:v 1280k -c:v libx264 -pix_fmt yuv420p -profile:v baseline -level 3.1 -c:a libfdk_aac -ac 2 -ar 48000 -b:a 128k "720p.mp4"

And for segmenting:

ffmpeg -y -async 1 -vsync -1 -analyzeduration 999999999 -i "720p.mp4" -codec copy -map 0 -f segment -segment_list "720p.m3u8" -segment_time 10 -segment_list_type m3u8 -bsf:v h264_mp4toannexb "segment%05d.ts"
```


### ExpressPlay Test Tools
- [Advanced Test Tool Guide](https://www.expressplay.com/developer/advanced-test-tool/)


### Test Content

- HLS: http://cdnapi.kaltura.com/p/1878761/sp/187876100/playManifest/entryId/1_usagz19w/flavorIds/1_5spqkazq,1_nslowvhp,1_boih5aji,1_qahc37ag/format/applehttp/protocol/http/a.m3u8

### Testing Sites

- Kaltura HLS [playback test page](http://player.kaltura.com/modules/KalturaSupport/tests/HLSPlaybackTester.html)


### Browser Support

We will actively support Google Chrome, Mozilla Firefox, IE9+. This is due to heavy use of ReactJS 
which has [discontinued support for React DOM](https://facebook.github.io/react/blog/2016/01/12/discontinuing-ie8-support.html) by the Facebook Team


### Troubleshooting

This section mostly applies to problems during playback:

- https://github.com/clappr/clappr/blob/master/doc/TROUBLESHOOTING.md#common-steps-to-verify-issues