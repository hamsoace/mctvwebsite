import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'
import ReactGA from 'react-ga'
import { createBrowserHistory} from 'history'

import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "./stylesheets/main.css";
import "./stylesheets/main-responsive.css";

import Home from "./pages/Home";
import DetailView from "./pages/DetailView";
import Playback from "./pages/Playback";

import Catalogue from "./pages/Catalogue";
import MyLibrary from "./pages/MyLibrary";
import ReleaseSchedule from "./pages/ReleaseSchedule";

import DownloadApp from "./pages/DownloadApp";

/* Account pages */
import Login from "./pages/account/Login";
import MyAccount from "./pages/account/MyAccount";
import ResetPassword from "./pages/account/ResetPassword";

/* Account pages */
import MookhPay from "./pages/payments/MookhPay";

/* Info pages */
import FaqPage from "./components/faq/Faq";
import About from './pages/About'
import CallForContent from './pages/info/CallForContent'
import CallForContentForm from './pages/info/CallForContentForm'
import TermsAndConditions from './pages/info/TermsAndConditions'
import Privacy from './pages/info/Privacy'

import Page404 from './pages/info/Page404'
import RequestMovie from "./pages/MovieRequest";

//ReactGA.initialize('UA-112778255-3')

ReactGA.initialize(
      {
        trackingId: 'UA-112778255-3',
        gaOptions: {
          siteSpeedSampleRate: 100,
        }
      },
  );

const history = createBrowserHistory()

class App extends Component {

    componentDidMount() {
        if (window.location.hostname !== 'localhost') {
            this.trackPageView()

            history.listen(() => {
                this.trackPageView()
            })
        }
    }

    trackPageView() {
        ReactGA.pageview(window.location.pathname)
    }

    render() {

        return (
            <Router>
                <div className="wrapper">
                    <Switch>
                        <Route exact path="/" component={Home} />

                        <Route path="/view/:id" component={DetailView} />
                        <Route path="/playback/:id" component={Playback} />

                        <Route path="/login" component={Login} />
                        <Route path="/reset-password" component={ResetPassword} />

                        <Route path="/catalogue" component={Catalogue} />
                        <Route path="/mymovies" component={MyLibrary} />
                        <Route path="/release-schedule" component={ReleaseSchedule} />

                        <Route path="/download-app" component={DownloadApp} />

                        <Route path="/my-account" component={MyAccount} />
                        <Route path="/mookhpay/:id" component={MookhPay} />

                        <Route path="/about" component={About} />
                        <Route path="/call-for-content" component={CallForContent} />
                        <Route path="/call-for-content-form" component={CallForContentForm} />
                        <Route path="/terms" component={TermsAndConditions} />
                        <Route path="/privacy" component={Privacy} />
                        <Route path="/movie-request" component={RequestMovie} />
                        <Route path="/faq" component={FaqPage} />

                        <Route component={Page404} />

                        {/* TODO: Remove the two movie urls; `/view` and `/playback` will be used */}
                        <Route path="/movie/view/:id" component={DetailView} />
                        <Route path="/movie/playback/:id" component={Playback} />
                    </Switch>
                </div>
            </Router>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps, null)(App)