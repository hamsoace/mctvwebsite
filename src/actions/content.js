import {getContent, getContentInfo, getGenres, getRecommendations, findContent} from '../utils/api';

export const FETCH_CONTENT = 'content/FETCH_CONTENT';
export const FETCH_CONTENT_PENDING = 'content/FETCH_CONTENT_PENDING';
export const FETCH_CONTENT_FULFILLED = 'content/FETCH_CONTENT_FULFILLED';
export const FETCH_CONTENT_REJECTED = 'content/FETCH_CONTENT_REJECTED';

export const SET_ACTIVE_CONTENT = 'content/SET_ACTIVE_CONTENT';

export const FETCH_CONTENT_INFO = 'content/FETCH_CONTENT_INFO';
export const FETCH_CONTENT_INFO_PENDING = 'content/FETCH_CONTENT_INFO_PENDING';
export const FETCH_CONTENT_INFO_FULFILLED = 'content/FETCH_CONTENT_INFO_FULFILLED';
export const FETCH_CONTENT_INFO_REJECTED = 'content/FETCH_CONTENT_INFO_REJECTED';

export const SEARCH_CONTENT = 'content/SEARCH_CONTENT';
export const SEARCH_CONTENT_PENDING = 'content/SEARCH_CONTENT_PENDING';
export const SEARCH_CONTENT_FULFILLED = 'content/SEARCH_CONTENT_FULFILLED';
export const SEARCH_CONTENT_REJECTED = 'content/SEARCH_CONTENT_REJECTED';

export const FETCH_GENRES = 'content/FETCH_GENRES';
export const FETCH_GENRES_PENDING = 'content/FETCH_GENRES_PENDING';
export const FETCH_GENRES_FULFILLED = 'content/FETCH_GENRES_FULFILLED';
export const FETCH_GENRES_REJECTED = 'content/FETCH_GENRES_REJECTED';

export const FETCH_RECOMMENDATIONS = 'content/FETCH_RECOMMENDATIONS';
export const FETCH_RECOMMENDATIONS_PENDING = 'content/FETCH_RECOMMENDATIONS_PENDING';
export const FETCH_RECOMMENDATIONS_FULFILLED = 'content/FETCH_RECOMMENDATIONS_FULFILLED';
export const FETCH_RECOMMENDATIONS_REJECTED = 'content/FETCH_RECOMMENDATIONS_REJECTED';

export const fetchContent = () => {
    const payload = getContent()

    return {type: FETCH_CONTENT, payload}
}

export const setActiveContent = (content) => {
    return {type: SET_ACTIVE_CONTENT, payload: content}
}

export const fetchContentInfo = (ref, user_id) => {
    const payload = getContentInfo(ref, user_id)

    return {type: FETCH_CONTENT_INFO, payload}
}

export const searchContent = (filter) => {
    const payload = findContent(filter)

    return {type: SEARCH_CONTENT, filter, payload}
}

export const fetchGenres = () => {
    const payload = getGenres()

    return {type: FETCH_GENRES, payload}
}

export const fetchRecommendations = (criteria) => {
    const payload = getRecommendations(criteria)

    return {type: FETCH_RECOMMENDATIONS, payload}
}