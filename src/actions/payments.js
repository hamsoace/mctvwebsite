import * as API from '../utils/api';

export const FETCH_PAYMENTS = 'payments/FETCH_PAYMENTS';
export const FETCH_PAYMENTS_PENDING = 'payments/FETCH_PAYMENTS_PENDING';
export const FETCH_PAYMENTS_FULFILLED = 'payments/FETCH_PAYMENTS_FULFILLED';
export const FETCH_PAYMENTS_REJECTED = 'payments/FETCH_PAYMENTS_REJECTED';

export const FETCH_PURCHASES = 'payments/FETCH_PURCHASES';
export const FETCH_PURCHASES_PENDING = 'payments/FETCH_PURCHASES_PENDING';
export const FETCH_PURCHASES_FULFILLED = 'payments/FETCH_PURCHASES_FULFILLED';
export const FETCH_PURCHASES_REJECTED = 'payments/FETCH_PURCHASES_REJECTED';

export const TOPUP_ACCOUNT = 'payments/TOPUP_ACCOUNT';
export const TOPUP_ACCOUNT_PENDING = 'payments/TOPUP_ACCOUNT_PENDING';
export const TOPUP_ACCOUNT_FULFILLED = 'payments/TOPUP_ACCOUNT_FULFILLED';
export const TOPUP_ACCOUNT_REJECTED = 'payments/TOPUP_ACCOUNT_REJECTED';

export const BUY_CONTENT = 'payments/BUY_CONTENT';
export const BUY_CONTENT_PENDING = 'payments/BUY_CONTENT_PENDING';
export const BUY_CONTENT_FULFILLED = 'payments/BUY_CONTENT_FULFILLED';
export const BUY_CONTENT_REJECTED = 'payments/BUY_CONTENT_REJECTED';


export const fetchUserPayments = (userId) => {
    const payload = API.getUserPayments(userId)

    return{
        type: FETCH_PAYMENTS,
        payload
    }
}

export const fetchUserPurchases = (userId) => {
    const payload = API.getUserPurchases(userId)

    return{
        type: FETCH_PURCHASES,
        payload
    }
}

export const topupAccount = (amount, mobile) => {
    const payload = API.topupAccount(amount, mobile)

    return{
        type: TOPUP_ACCOUNT,
        payload
    }
}

export const initBuyContent = (buyObj) => {
    const payload = API.buyContent(buyObj)

    return{
        type: BUY_CONTENT,
        payload
    }
}