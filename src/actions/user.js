import {
    login,
    updateProfile,
    getUserBundle,
    getUserWallet,
    getUserFavourites,
    toggleFavourites,
    getUserDownloads,
} from '../utils/api';

export const USER_LOGIN = 'user/USER_LOGIN';
export const USER_LOGIN_PENDING = 'user/USER_LOGIN_PENDING';
export const USER_LOGIN_FULFILLED = 'user/USER_LOGIN_FULFILLED';
export const USER_LOGIN_REJECTED = 'user/USER_LOGIN_REJECTED';

export const UPDATE_USER = 'user/UPDATE_USER';
export const UPDATE_USER_PENDING = 'user/UPDATE_USER_PENDING';
export const UPDATE_USER_FULFILLED = 'user/UPDATE_USER_FULFILLED';
export const UPDATE_USER_REJECTED = 'user/UPDATE_USER_REJECTED';

export const FETCH_BUNDLE = 'user/FETCH_BUNDLE';
export const FETCH_BUNDLE_PENDING = 'user/FETCH_BUNDLE_PENDING';
export const FETCH_BUNDLE_FULFILLED = 'user/FETCH_BUNDLE_FULFILLED';
export const FETCH_BUNDLE_REJECTED = 'user/FETCH_BUNDLE_REJECTED';

export const FETCH_WALLET = 'user/FETCH_WALLET';
export const FETCH_WALLET_PENDING = 'user/FETCH_WALLET_PENDING';
export const FETCH_WALLET_FULFILLED = 'user/FETCH_WALLET_FULFILLED';
export const FETCH_WALLET_REJECTED = 'user/FETCH_WALLET_REJECTED';

export const FETCH_FAVOURITES = 'user/FETCH_FAVOURITES';
export const FETCH_FAVOURITES_PENDING = 'user/FETCH_FAVOURITES_PENDING';
export const FETCH_FAVOURITES_FULFILLED = 'user/FETCH_FAVOURITES_FULFILLED';
export const FETCH_FAVOURITES_REJECTED = 'user/FETCH_FAVOURITES_REJECTED';

export const TOGGLE_FAVOURITES = 'user/TOGGLE_FAVOURITES';
export const TOGGLE_FAVOURITES_PENDING = 'user/TOGGLE_FAVOURITES_PENDING';
export const TOGGLE_FAVOURITES_FULFILLED = 'user/TOGGLE_FAVOURITES_FULFILLED';
export const TOGGLE_FAVOURITES_REJECTED = 'user/TOGGLE_FAVOURITES_REJECTED';

export const FETCH_DOWNLOADS = 'user/FETCH_DOWNLOADS';
export const FETCH_DOWNLOADS_PENDING = 'user/FETCH_DOWNLOADS_PENDING';
export const FETCH_DOWNLOADS_FULFILLED = 'user/FETCH_DOWNLOADS_FULFILLED';
export const FETCH_DOWNLOADS_REJECTED = 'user/FETCH_DOWNLOADS_REJECTED';

export const loginUser = (user) => {
    const payload = login(user)

    return{
        type: USER_LOGIN,
        payload
    }
}

export const updateUser = (user) => {
    const payload = updateProfile(user)

    return {
        type: UPDATE_USER,
        payload
    }
}

export const fetchBundle = (userId) => {
    const payload = getUserBundle(userId)

    return {
        type: FETCH_BUNDLE,
        payload
    }
}

export const fetchWallet = (userId) => {
    const payload = getUserWallet(userId)

    return{
        type: FETCH_WALLET,
        payload
    }
}

export const fetchFavourites = (userId) => {
    const payload = getUserFavourites(userId)

    return{
        type: FETCH_FAVOURITES,
        payload
    }
}

export const toggleUserFavourites = (favObj) => {
    const payload = toggleFavourites(favObj)

    return{
        type: TOGGLE_FAVOURITES,
        payload
    }
}

export const fetchUserDownloads = (userId) => {
    const payload = getUserDownloads(userId)

    return{
        type: FETCH_DOWNLOADS,
        payload
    }
}