import React, { Component } from 'react'
import Slider from 'react-slick'

import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'

/**
 * 
 * Carousel component: Creates a slider to display banner content
 * 
 * https://github.com/akiran/react-slick
 * 
 */
export default class Carousel extends Component{

    render(){
        
        let settings = {
            fade: this.props.fade || false,
            autoplay: this.props.autoPlay || false,
            autoplaySpeed: this.props.autoplaySpeed || 4000,
            speed: this.props.speed || 500,
            dots: this.props.dots || false,
            infinite: this.props.infinite || true,
            slidesToShow: this.props.slidesToShow || 1,
            slidesToScroll: this.props.slidesToScroll || 1,

            arrows: this.props.arrows || true,
            prevArrow: '<button>Previous</button>',
            nextArrow: '<button>Next</button>'
        }

        settings = {settings, ...this.props}

        /**
         * 
         * Some gotchas when using this element:
         * 
         * - Make sure each child being rendered is inside a <div>
         * - The enclosing <div> should have a `key` param
         * 
         */
        return (
            <Slider { ...settings }>
                { this.props.children }
            </Slider>
        );
    }
}