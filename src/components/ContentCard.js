import React, {Component} from 'react'
import {NavLink} from 'react-router-dom'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {getArtwork} from '../utils/content'

import {setActiveContent} from '../actions/content'

class ContentCard extends Component {

    constructor(props) {
        super(props)

        this.state = {}
    }

    render() {
        // console.log(this.props.contentInfo)

        const {ref, title, synopsis, purchase_type, elapsed = 0, duration = 0} = this.props.contentInfo
        //const contentCardClass = this.props.className || "col-xs-6 col-sm-3 col-md-2"
        const contentCardClass = this.props.className

        const timeDiff = (duration * 60) - parseInt(elapsed)
        const elapsedPercentage = elapsed ? timeDiff / (duration * 60) * 100 : 0

        return (
            <div className={contentCardClass}>
                <div className="content-card">

                    <NavLink
                        to={`/view/${ref}`}
                        onClick={(e) => {
                            this.props.onBtnClick && this.props.onBtnClick(e)
                            this.props.setActiveContent(this.props.contentInfo)
                        }}>

                        {
                            this.props.hasPlayButton ?
                                <div className="play-button-overlay"/>
                                : null
                        }

                        {
                            purchase_type ?
                                <span
                                    className={`purchase-label ${purchase_type.toLowerCase()}`}>
                                        {purchase_type}
                                </span>
                                : null
                        }

                        <img
                            src={getArtwork(ref).portrait}
                            alt={title}
                            className="content-card-img img-responsive"/>

                        <div className="clearfix content-card-info">
                            <h5 className="hidden">{title}</h5>
                            <p>
								<span
                                    className="synopsis"
                                    dangerouslySetInnerHTML={{__html: synopsis}}/>

                                <span>
									<button className="btn btn-danger btn-sm btn-block content-card-button hidden">
										<i className="icon ion-play"/> Watch
									</button>
								</span>
                            </p>
                        </div>

                        <div className="elapsed-progress"
                             style={{width: `${elapsedPercentage}%`}}/>
                    </NavLink>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        content: state.content
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({setActiveContent}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ContentCard)
