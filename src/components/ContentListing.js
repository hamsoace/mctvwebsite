import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import Carousel from './Carousel'
import ContentCard from './ContentCard'
import Loader from './Loader'

import {fetchContent} from '../actions/content'

class ContentListing extends Component {

    constructor(props) {
        super(props)

        this.state = {}
    }

    UNSAFE_componentWillReceiveProps(nextProps){
        // console.log(nextProps)
    }

    componentDidMount() {
        const { content = [] } = this.props.content.contentList;

        /* TODO: Setup sync check with ETags */
        if (content.length === 0) this.props.fetchContent()
    }

    render() {
        // console.log(this.props)

        const { contentList, contentListError = false, contentListLoading} = this.props.content
        const {content=[], genres=[]} = contentList

        return (
            <div className="clearfix">
                {content.length > 0 && !contentListLoading ? 
                    <div className="clearfix">
                    {
                        genres.map((genre, index) => {

                            return <div className="clearfix content-genre-wrapper" key={index}>
                                <h4 className="content-genre-header">{genre}</h4>
                                <div className="clearfix">
                                    
                                    <Carousel
                                        arrows={true}
                                        infinite={false}
                                        slidesToShow={6}
                                        slidesToScroll={3}
                                        responsive={
                                            [
                                                {
                                                    breakpoint: 768,
                                                    settings: {
                                                        slidesToShow: 4,
                                                        slidesToScroll: 4
                                                    }
                                                },
                                                {
                                                    breakpoint: 480,
                                                    settings: {
                                                        slidesToShow: 2,
                                                        slidesToScroll: 2
                                                    }
                                                }
                                            ]
                                        }>
                                        
                                        {
                                            content.map((ct, i) => {
                                                const contentGenres = JSON.parse(ct.genres)
                                                
                                                if(contentGenres.indexOf(genre) !== -1){
                                                    return (
                                                        <div key={i}>
                                                            <ContentCard
                                                                contentInfo={ct}
                                                                key={i} />
                                                        </div>
                                                    )
                                                }
                                                else{
                                                    return null
                                                }
                                            })
                                        }
                                    </Carousel>
                                </div>
                            </div>
                        })
                    }
                    </div>
                    : 
                    <div>
                        {
                            contentListLoading ?
                                <Loader message="" />
                            : null
                        }
                    </div>
                }

                {
                    contentListError ? 
                        <div className="alert alert-info text-center">
                            Could not fetch content. &nbsp;
                            <button 
                                className="btn btn-sm btn-warning"
                                onClick={() => { this.props.fetchContent() }}>Refresh</button>
                        </div>
                    :
                    null
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        content: state.content, 
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        fetchContent
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ContentListing)