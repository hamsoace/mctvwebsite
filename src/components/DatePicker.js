import React, { Component } from 'react'

import {format, subYears} from 'date-fns'

export default class DatePicker extends Component {

    constructor(props){
        super(props)

        // Set today's date as default
        const selectedDate = this.parseDate(this.props.value)

        this.state = { selectedDate }
    }

    parseDate = (dateStr) => {

        // Set default date as 13 years ago
        const date = subYears(new Date(), 13)

        const d = dateStr ? new Date(dateStr) : date

        return {
            day: d.getDate() || 1,
            month: d.getMonth() + 1 || 1,
            year: d.getFullYear() || 1990
        }
    }

    range = (start, end) => {
        let range = []

        if(start > end){
            console.error('Range end must be greater')
        }
        else{
            for (let i = start; i <= end; i++) {
                range.push(i)
            }
        }

        return range
    }

    onChange = (e) => {
        e.preventDefault()

        const { name, value } = e.target
        
        // Get integer value of date values
        const intVal = parseInt(value)

        const { selectedDate } = this.state

        const Time = {
            DAY: 'day',
            MONTH: 'month',
            YEAR: 'year'
        }

        let date = {}

        /**
         * 
         * Check for months with 30 days: April, June, September, November. Check from
         * an array of number value of the month.
         * 
        */
        const is30day = [4, 6, 9, 11].filter((m) => { return m === intVal }).length > 0

        if(name === Time.MONTH && is30day && intVal > 30){
            console.log('opt1')
            date = {
                ...selectedDate,
                day: 30
            }
        }

        /**
         * 
         * Check for February. Resets the day selected to:
         * 
         * - 28 if it's a non-leap year
         * - 29 if it's a leap year
         * 
         * If the selected date is greater than what the month allows.
         * 
         * */
        else if(name === Time.MONTH && intVal === 2 && selectedDate.day > 28){
            const day = selectedDate.year % 4 === 0 ? 29 : 28

            date = {
                ...selectedDate,
                day
            }
        }
        else if (name === Time.DAY && selectedDate.month === 2 && intVal > 28) {
            const day = selectedDate.year % 4 === 0 ? 29 : 28

            date = {
                ...selectedDate,
                day
            }
        }

        else{
            date = {
                ...selectedDate,
                [name]: intVal
            }
        }

        this.setState({ selectedDate: date})

        const { day, month, year } = this.state.selectedDate

        const formattedDate = format(new Date(year, month - 1, day), 'yyyy-MM-dd 00:00:00')
        this.props.onDatePicked(formattedDate)
    }

    render() {
        // console.log(this.state.selectedDate)

        const { currentYear, yearBuffer } = this.props
        const yearDiff = currentYear - yearBuffer
        

        const { day = 1, month = 1, year = yearDiff } = this.state.selectedDate

        const days = this.range(1, 31)
        const months = this.range(1, 12)
        const years = this.range(yearDiff, currentYear)

        const style = {
            background:'#555',
            border:0,
            color: '#ddd',
            cursor: 'pointer',
            float: 'left',
            padding:'10px 3px',
            width: '33%'
        }

        return (
            <div 
                className="clearfix" 
                style={{ overflow:'hidden', borderRadius:'3px' }}>

                <select 
                    name="day" 
                    onChange={this.onChange}
                    value={day}
                    style={style}
                    required>

                    <option value="">Day</option>
                    
                    {
                        days.map((d, i)=>{
                            return(
                                <option key={i} value={d}>{d}</option>
                            )
                        })
                    }
                </select>

                <select 
                    name="month"
                    onChange={this.onChange}
                    value={month}
                    style={style}
                    required>

                    <option value="">Month</option>

                    {
                        months.map((m, i) => {
                            return (
                                <option key={i} value={m}>{m}</option>
                            )
                        })
                    }
                </select>
                
                <select 
                    name="year"
                    onChange={this.onChange}
                    value={year}
                    style={style}
                    required>

                    <option value="">Year</option>
                    
                    {
                        years.map((y, i) => {
                            return (
                                <option key={i} value={y}>{y}</option>
                            )
                        })
                    }
                </select>
            </div>
        )
    }
}

DatePicker.defaultProps = {
    yearBuffer: 80,
    currentYear: new Date().getFullYear()
}