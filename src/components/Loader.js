import React, {Component} from 'react';

import loaderGif from '../images/loader.gif';

export default class Loader extends Component {

    render() {

        return (
            <div className="clearfix text-center loader">
                <img src={loaderGif} className="img-responsive" alt="Loading..." />
                <p>{this.props.message}</p>
            </div>
        );
    }
}