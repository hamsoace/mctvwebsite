import React, { Component } from 'react'

import { auth } from '../utils/firebaseAuth'

export default class LoginRedirect extends Component {

    constructor(){
        super()

        this.state = {
            user: null
        }
    }

    componentDidMount(){
        this.checkRedirect()
    }

    checkRedirect(){
        auth.onAuthStateChanged((user) => {
            if (user) {
                this.setState({ user });
                this.props.history.push('/login')
            }
            else{
                this.props.history.push('/')
            }
        })
    }

    render () {

        return(
            <div></div>
        );
    }
}