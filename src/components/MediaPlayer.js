import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Clappr from 'clappr'
import ChromecastPlugin from 'clappr-chromecast-plugin'

import LevelSelector from '../plugins/clappr-level-selector/level-selector.jsx'
import MarkersPlugin from '../plugins/clappr-markers/markers-plugin.js'
import '../stylesheets/media-player.css'

import { requestStream, storeView } from '../utils/api'
import { getArtwork } from '../utils/content'

class MediaPlayer extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isPlaying: false,
            isChromecasting: false,
            nightMode: this.props.nightMode || false,
            player: {}
        }
    }

    componentDidMount() {
        const streamObj = {
            user_id: '',
            ref: this.props.contentRef
        }

        requestStream(streamObj).then((response) => {
            // console.log('STREAM', response)
            this.initPlayer(response.data.url)
        })
    }

    componentWillUnmount() {
        const { player } = this.state
        const { accountInfo } = this.props.user

        /* Save viewed time. Will be used under the continue watching section in home page, or my library  */
        // console.log('PLAYBACK TIME: ', this.state.player.getCurrentTime())

        const view = {
            user_id: accountInfo.id,
            ref: this.props.contentRef,
            tm: player.getCurrentTime()
        }

        storeView(view)
    }

    initPlayer = (mediaURL) => {
        const { contentRef, contentTitle } = this.props

        const playerConfig = {
            parentId: '#player',
            loop: false,
            source: mediaURL,
            gaAccount: 'UA-112778255-3',
            gaTrackerName: 'WebStreamingPlayer',
            poster: getArtwork(contentRef).portrait,
            plugins: {
                core: [ChromecastPlugin, MarkersPlugin, LevelSelector]
            },
            chromecast: {
                appId: '02158EDB',
                contentType: 'video/mp4',
                media: {
                    type: ChromecastPlugin.Movie,
                    title: contentTitle,
                    subtitle: 'MyMoviesAfrica'
                }
            },
            levelSelectorConfig: {
                title: 'Quality',
                labels: {
                    0: '240p', // 120kbps
                    1: '480p', // 240kbps
                    2: '720p', // 500kbps
                    3: '1080p' //
                },
                /*labelCallback: function(playbackLevel, customLabel) {
                    console.log(playbackLevel, customLabel)
                    return customLabel + playbackLevel.level.height+'p' // High 720p
				}
				*/
            },
            markersPlugin: {
                markers: [new MarkersPlugin.StandardMarker(0, "Start")],
                tooltipBottomMargin: 17
            }
        }

        const player = new Clappr.Player(playerConfig)
        // console.log(player)
        player.play()

        this.setState({ player })
    }

    render() {
        const mediaPlayerStyle = {
            background: '#222',
            cursor: 'pointer',
            height: '600px',
            width: '100%'
        }

        return (
            <div
                className="media-player"
                style={{
                    mediaPlayerStyle
                }}>

                <div id="player" style={{
                    width: "100%"
                }}></div>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return { content: state.content, user: state.user }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(MediaPlayer)