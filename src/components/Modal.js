import React, { Component } from 'react';

export default class Modal extends Component {

    onModalClose = (e) => {
        e.preventDefault();

        if (this.props.hasOwnProperty('onModalClose')) {
            this.props.onModalClose(e);
        }
    }

    onModalOpen = (e) => {
        e.preventDefault();

        if (this.props.hasOwnProperty('onModalOpen')) {
            this.props.onModalOpen(e);
        }
    }

    render() {
        const {
            modalId,
            title,
            children,
            closable = '',
            modalClass = '',
            buttonClass = '',
            buttonStyle = {}
        } = this.props

        return (
            <span className="clearfix">
                <button
                    type="button"
                    onClick={this.onModalOpen}
                    style={buttonStyle}
                    className={`${buttonClass}`}
                    data-toggle="modal"
                    data-target={`#${modalId}`}>{title}</button>

                <div
                    className={"modal " + modalClass}
                    tabIndex="-1"
                    role="dialog"
                    id={`${modalId}`}>

                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                {
                                    closable === true || closable === '' ?
                                        <button
                                            type="button"
                                            className="close"
                                            data-dismiss="modal"
                                            aria-label="Close"
                                            onClick={this.onModalClose}>

                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        :
                                        null
                                }
                                <h4 className="modal-title">{title}</h4>
                            </div>
                            <div className="modal-body clearfix">{children}</div>
                        </div>
                    </div>
                </div>
            </span>
        );
    }
}