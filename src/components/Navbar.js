import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import ContentCard from '../components/ContentCard'

import { auth } from '../utils/firebaseAuth.js'
import logo from '../images/mma-logo.png'

import { clearUserSession, getUserDetails, isLoggedIn } from '../utils/user-utils'

import { searchContent, fetchGenres } from '../actions/content'

class Navbar extends Component{

    constructor(props){
        super(props)

        this.state = {

            // Search state values
            filter: {},
            results:{},
            searchOpen: false,
            collapsed: true,

            user: getUserDetails()
        }
    }

    UNSAFE_componentWillReceiveProps(nextProps){

        if(!isLoggedIn()){
            this.setState({ user: {} })
        }
    }

    logout = (e) => {
        auth.signOut()
            .then(() => {
                clearUserSession()
                
                this.setState({ user: {} })
            })
    }

    searchContent = () => {
        this.props.searchContent(this.state.filter)
    }

    toggleSearch = (e) => {
        e.preventDefault()
        e.stopPropagation()
        
        this.setState({
            searchOpen: !this.state.searchOpen
        })
    }

    toggleNavbar = () => {
        this.setState({
            collapsed: !this.state.collapsed
        })
    }

    onChange = (e) => {
        e.preventDefault()

        const { name, value } = e.target
        const { query } = this.state

        this.setState({
            filter: {
                ...query,
                [name]: value
            }
        }, ()=>{
            this.searchContent()
        })
    }

    render(){
        // console.log(this.props)
        // console.log(this.state)

        const { content } = this.props
        const { user, filter, searchOpen, collapsed } = this.state

        const navbarClass = collapsed ? 
            'navbar-collapse collapse' : 
            'navbar-collapse collapse in'

        return(
            <nav className="navbar navbar-inverse">
                <div className="container">
                    <div className="navbar-header">
                        <button 
                            type="button" 
                            className="navbar-toggle"
                            data-toggle="collapse" 
                            data-target="#bnav-collapse" 
                            aria-expanded="false"
                            onClick={this.toggleNavbar}>

                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>

                        <NavLink to="/" className="navbar-brand">
                            <img 
                                src={ logo } 
                                className="img-reponsive" 
                                alt="MyMoviesAfrica" />
                        </NavLink>
                    </div>

                    <div className={navbarClass} id="nav-collapse">
                        <ul className="nav navbar-nav">
                            <li><NavLink exact to="/" activeClassName="active">Home</NavLink></li>
                            <li><NavLink to="/mymovies" activeClassName="active">MyMovies</NavLink></li>
                            {
                                false ? 
                                    <li><NavLink to="/release-schedule" activeClassName="active">Releases</NavLink></li> 
                                : null
                            }
                            { /*<li><NavLink to="/download-app" activeClassName="active">Download App</NavLink></li> */}
                            <li><NavLink to="/call-for-content">Call for Content</NavLink></li>
                            { /*<li><NavLink to="/movie-request">Movie Request</NavLink></li> */}
                            { /*<li><NavLink to="/faq">FAQ</NavLink></li> */}
                        </ul>
                        <ul className="nav navbar-nav navbar-right">
                            <li>
                                <NavLink 
                                    to="/search"
                                    onClick={ this.toggleSearch }>

                                    <i className="icon ion-md-search"></i>
                                </NavLink>
                            </li>

                            { 
                                user.email ?
                                
                                <li className="dropdown">
                                    <a href="/" className="dropdown-toggle" data-toggle="dropdown" role="button" 
                                        aria-haspopup="true" aria-expanded="false">
                                        { user.fullname } <span className="caret"></span>
                                    </a>
                                    <ul className="dropdown-menu profile-dropdown-menu">
                                        <li><NavLink to="/my-account">My Account</NavLink></li>
                                        <li>
                                            <NavLink to="/" onClick={ this.logout }>Logout</NavLink>
                                        </li>
                                    </ul>
                                </li>
                            :
                                <li>
                                    <NavLink to="/login" className="btn btn-nav btn-warning">
                                        Login
                                    </NavLink>
                                </li>
                            }
                        </ul>
                    </div>
                </div>

                <div 
                    className={ "clearfix search-overlay " + (searchOpen ? '' : 'hidden' ) }>

                    <form className="form-inline">
                        <span
                            className="close-search"
                            onClick={ this.toggleSearch }>
                            &times;
                        </span>

                        <input
                            type="search"
                            className="form-control search-input"
                            name="query"
                            placeholder="Enter a title to search..."
                            onChange={ this.onChange }
                            autoFocus={ !this.state.searchOpen }
                            autoComplete="off" />
                        
                        <label className="checkbox hidden">
                            <input
                                name="genre"
                                type="checkbox"
                                onChange={ this.onChange } /> 
                        </label>

                        <div className="clearfix search-results row">
                        {
                            content.searchResults.results.length > 0 && filter.query ?

                                content.searchResults.results.map((content, i) => {
                                    
                                    return (
                                        <ContentCard
                                            className="col-xs-6 col-sm-3"
                                            key={ i } 
                                            contentInfo={ content } 
                                            onBtnClick ={this.toggleSearch}/>
                                    )
                                })
                            :
                                <div>
                                {
                                    filter.query && content.searchResults.results.length > 0 ?
                                        <div className="clearfix blank-state">
                                            No results found
                                        </div>
                                    : null
                                }
                                </div>
                        }
                        </div>
                    </form>
                </div>
            </nav>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        content: state.content,
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ searchContent, fetchGenres }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Navbar)