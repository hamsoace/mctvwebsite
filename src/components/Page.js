import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { Helmet } from 'react-helmet'
import $ from 'jquery'

// import FirebaseAuth from '../utils/firebaseAuth.js'

import browserUpdate from '../utils/browserUpdate'
import Navbar from '../components/Navbar'

import { auth } from '../utils/firebaseAuth'
import { loginUser } from '../actions/user'

class Page extends Component {

    componentDidMount() {
        /* Notify user that browser is out of date  */
        browserUpdate()

        if (!this.props.preserveScroll) {
            $('body').scrollTop(0)
        }

        if(window.location.hostname !== 'localhost'){

            /**
             * 
             * Init chat
             * 
            */
            /*Removed jivo chat*/
            (function () {
                var widget_id = '4LYFtoGjRC'; var d = document; var w = window; function l() {
                    var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true;
                    s.src = '//code.jivosite.com/script/widget/' + widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);
                }
                if (d.readyState === 'complete') { l(); } else {
                    if (w.attachEvent) { w.attachEvent('onload', l); }
                    else { w.addEventListener('load', l, false); }
                }
            })()
        }
    }

    checkLogin() {
        const { accountInfo } = this.props.user

        auth.onAuthStateChanged((user) => {

            if (user) {
                this.setState({ user })

                const { displayName, email, phoneNumber, photoURL } = user

                const userObj = {
                    name: displayName,
                    email,
                    phone_number: phoneNumber,
                    photo_url: photoURL,
                    birthday: ''
                }

                if (!accountInfo.hasOwnProperty('id')) {
                    this.props.loginUser(userObj)
                }
            } else {
                console.log('User not logged in')
            }
        })
    }

    render() {
        const {
            pageTitle = 'MyMoviesAfrica™',
            pageImage = 'http://mymoviesafrica.org/img/logo.png',
            fullScreen,
            showPageTitle,
            children,
        } = this.props

        const pageURL = window.location.href

        /**
         * ogArray contains info to create Open Graph tags
         *
         */
        //const ogArray = this.props.ogArray || []

        return (
            <div className="clearfix">
                {!fullScreen
                    ? <Navbar />
                    : null}

                <div
                    className={"clearfix page-container container " + (fullScreen
                        ? ' fullscreen'
                        : '')}>

                    <Helmet titleTemplate="%s">
                        <meta charSet="utf-8" />
                        <meta property="og:title" content={`${pageTitle} | MyMoviesAfrica™`} />
                        <meta property="og:type" content="video.movie" />
                        <meta property="og:url" content={pageURL} />
                        <meta property="og:image" content={pageImage} />

                        <title>{`${pageTitle} | MyMoviesAfrica™`}</title>
                        <link rel="canonical" href={pageURL} />
                    </Helmet>
                    {
                        pageTitle && showPageTitle ?
                            <div className="page-header">
                                <h1>{pageTitle}</h1>
                            </div>
                            : null
                    }

                    <div className="clearfix">
                        {children}
                    </div>
                </div>

                {!fullScreen
                    ? <div className="footer container">
                        <div className="col-sm-12">
                            <NavLink to="/">Home</NavLink>
                            <NavLink to="/about">About MyMoviesAfrica&trade;</NavLink>
                            <NavLink to="/call-for-content">Call for Content</NavLink>
                            {
                                //<NavLink to="/terms">Terms</NavLink>
                            }
                            <NavLink to="/privacy">Privacy</NavLink>
                        </div>
                    </div>
                    : null
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return { content: state.content, user: state.user }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        loginUser
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Page)