import React, { Component } from 'react'

import URL from '../utils/urls'

export default class PaymentsModal extends Component {

    render() {
        const { id, phone } = this.props.user

        return(
            <div className="clearfix">
                <button className="btn btn-block btn-success btn-payment" 
                    data-toggle="modal" data-target="#pmodal">
                    <i className="icon ion-md-card"></i> Top-up your account
                </button>

                <div className="modal" id="pmodal" 
                    tabIndex="-1" role="dialog" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 className="modal-title" id="myModalLabel">Top-up your Account</h4>
                            </div>
                            <div className="modal-body col-sm-12">

                                {
                                    phone !== '' ? 

                                        <div className="clearfix">
                                            <iframe
                                                title="mookhPay"
                                                src={`${URL.baseAPI}${URL.payGate}/${id}`}
                                                id="mookh-frame"
                                                frameBorder="0"
                                                style={{ height: '450px', width: '100%' }}></iframe>
                                        </div>
                                    : 
                                        <div className="col-sm-12 alert alert-danger">
                                            <i className="icon ion-md-information-circle"></i>&nbsp;
                                            <span style={{ display: 'inline-block', paddingTop: '0.3em' }}>
                                                Update your phone number to enable mobile payments
                                            </span>
                                        </div>
                                }
                            </div>
                            <div className="modal-footer">
                                {/* <button 
                                    type="button" 
                                    className="btn btn-success" 
                                    data-dismiss="modal"
                                    onClick={()=>{ this.props.onCloseModal() }}>Finish
                                </button> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}