import React, { Component } from 'react'

import em1 from '../images/emojis/em-1.png'
import em2 from '../images/emojis/em-2.png'
import em3 from '../images/emojis/em-3.png'
import em4 from '../images/emojis/em-4.png'
import em5 from '../images/emojis/em-5.png'

export default class RatingEmojis extends Component {

    render() {
        const { rating=1 } = this.props

        const emojis = [em1, em2, em3, em4, em5];

        const ratingNum = Number.parseInt(rating);

        return (
            <span className="rating-emojis">
            {
                emojis.map((emoji, i) => {
                    const isActiveEmoji = (i + 1 === ratingNum)
                        ? ' active-emoji'
                        : ''

                    return (
                        <span className={'rating-emoji' + isActiveEmoji} key={i}>
                            <img src={emoji} className="img-responsive" alt="Rating emoji" />
                        </span>
                    )
                })
            }
            </span>
        );
    }
}