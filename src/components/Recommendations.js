import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import ContentCard from './ContentCard';

import { fetchRecommendations } from '../actions/content';
/**
 *
 * Recommendations Component
 *
 * State:
 * @var {count} [Number of recommendations to be shown]
 *
 */
class Recommendations extends Component {

    constructor(props) {
        super(props);

        this.state = {
            count: 4
        };
    }

    componentDidMount() {
        const { contentRef, genres, fetchRecommendations } = this.props;
        const criteria = {
            ref: contentRef,
            genres
        }

        if (contentRef){
            fetchRecommendations(criteria)
        }
    }

    render() {
        const { recommendations } = this.props.content

        return (
            <div className="clearfix recommendations-holder">
                <h4>You may also like:</h4>

                <div className="row clearfix">
                    {
                        recommendations.map((content, i) => {
                            return (
                                <ContentCard
                                    key={i}
                                    className="col-xs-6 col-sm-3 col-md-2"
                                    contentInfo={content} />
                            )
                        })
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        content: state.content, 
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        fetchRecommendations
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Recommendations);