import React, { Component } from 'react'
import $ from 'jquery'
import Swal from 'sweetalert2/dist/sweetalert2.js'
import videojs from 'video.js'
import "videojs-contrib-quality-levels";
import "videojs-contrib-eme";
import chromeLogo from '../images/browsers/chrome.png'
import firefoxLogo from '../images/browsers/firefox.png'
import { requestStream, setStreamProgress } from '../utils/api'
import { isSupported } from '../utils/ua.js'
import { getUserDetails } from '../utils/user-utils'
import DB from '../utils/db'
import canAutoplay from 'can-autoplay'
import '../stylesheets/videojs-custom.css'
import 'sweetalert2/dist/sweetalert2.min.css'
import 'video.js/dist/video-js.css'
/**
 *
 * Video player powered by VideoJS
 *
 * https://github.com/videojs/video.js/blob/master/docs/guides/react.md
 *
 */
export default class Video extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: getUserDetails(),
            elapsedTimeInSeconds: 0,
        }
    }
    initPlayer = () => {
        const { contentRef, title, subtitle } = this.props
        const { id } = this.state.user
        const _this = this;
        const options = {
            controls: true,
            playbackRates: [0.5, 1, 1.5, 2],
            chromecast: {
                appId: '02158EDB',
                metadata: {
                    title: title,
                    subtitle: subtitle
                }
            },
            audioCapabilities: [],
            videoCapabilities: [
                {
                    robustness: 'HW_SECURE_ALL',
                    contentType: 'video/mp4;codecs="avc1.42E01E"'
                }
            ]
        }
        this.player = videojs(this.videoNode, options, function onPlayerReady() {
            // console.log('onPlayerReady', this)
            this.eme()
            this.qualityLevels()
            requestStream({ ref: contentRef, user_id: id, stype: 'web' })
                .then((response) => {
                    const { message, error = false, lc, url, view = {} } = response.data
                    // console.log(response.data.view)
                    if (message && error === true) {
                        Swal.fire({
                            title: '',
                            text: message,
                            icon: 'error',
                            confirmButtonText: 'OK'
                        })
                    }
                    else {
                        this.src({
                            // src:' http://www.bok.net/dash/tears_of_steel/cleartext/stream.mpd',
                            // src: 'http://wams.edgesuite.net/media/MPTExpressionData02/BigBuckBunny_1080p24_IYUV_2ch.ism/manifest(format=mpd-time-csf)',
                            src: url,
                            type: 'application/dash+xml',
                            // src: 'http://sample.vodobox.net/skate_phantom_flex_4k/skate_phantom_flex_4k.m3u8',
                            // type: 'application/x-mpegURL',
                            // src: 'https://cdn.mymovies.africa/stage/test/video.mp4',
                            keySystems: {
                                'com.widevine.alpha': lc
                            }
                        })

                        canAutoplay.video().then(function (obj) {
                            if (obj.result === true) {
                                _this.player.play()
                            }
                        });
                        // Set start time once metadata is loaded
                        this.on('loadedmetadata', () => {
                            if (view.elapsed === undefined) {
                                // set the movie to start playing from 5 seconds so that autoplay kicks in
                                // TODO consider having a play button instead of autoplaying from 0:05
                                // this.currentTime(0)
                                this.bigPlayButton.show()
                            } else {
                                this.currentTime(view.elapsed)
                            }
                        })
                        // console.log(player.qualityLevels())
                        this.on('useractive', (e) => {
                            $('.media-player-bar').show(200)
                        })
                        this.on('userinactive', (e) => {
                            $('.media-player-bar').hide(1500)
                        })
                        this.on('seeked', () => {
                            _this.sendViewProgress()
                        })
                        this.on('timeupdate', (e) => {
                            _this.sendViewProgress()
                        })
                        this.ended('ended', (e) => {
                            _this.sendViewProgress()
                        })
                        // this.setState({ player:this.player })
                    }
                }).catch(err=>{
                console.log(err)
            })
        })
    }
    componentDidMount = () => {
        this.initPlayer()
        if (isSupported()) {
            // show logs in localhost
            if (window.location.hostname !== 'localhost') {
                this.player.log.level('off')
            }
        }
    }
    UNSAFE_componentWillUnmount = () => {
        const { contentRef } = this.props
        const elapsed = DB.fetch(contentRef)
        this.sendViewProgress(elapsed)
        if (this.player) this.player.dispose()
    }
    sendViewProgress = () => {
        const { elapsedTimeInSeconds } = this.state
        if (!this.player) {
            this.componentDidMount()
        }
        const { id } = this.state.user
        const currentTime = Math.round(this.player.currentTime() || 0)
        const elapsedModulus = elapsedTimeInSeconds % 30
        // const duration = player.duration()
        if (currentTime > elapsedTimeInSeconds
            && elapsedModulus === 0
            && currentTime > 10) {
            DB.insert(currentTime)
            setStreamProgress({ ref: this.props.contentRef, elapsed: currentTime, user_id: id })
        }
        if (currentTime > elapsedTimeInSeconds && currentTime % 10 === 0) {
            this.setState({ elapsedTimeInSeconds: currentTime })
        }
    }
    render() {
        // console.log(this.state, 'SUPPORTED: ' + isSupported())
        const style = { height: '100vh', width: '100%' }
        return (
            <div className="clearfix">
                {
                    !isSupported() ?
                        <div className="video-alert">
                            <div className="row">
                                <div className="col-sm-12">
                                    <p>Your browser is not supported. Please try the following browsers:</p>
                                </div>
                                <div className="col-sm-6">
                                    <a href="https://www.google.com/chrome/">
                                        <img
                                            src={chromeLogo}
                                            alt="Google Chrome"
                                            className="img-responsive" />
                                        <span>Google Chrome</span>
                                    </a>
                                </div>
                                <div className="col-sm-6">
                                    <a href="https://www.mozilla.org/en-US/firefox/new/">
                                        <img
                                            src={firefoxLogo}
                                            alt="Mozilla Firefox"
                                            className="img-responsive" />
                                        <span>Mozilla Firefox</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        :
                        <div data-vjs-player>
                            <video
                                ref={node => this.videoNode = node}
                                className="video-js vjs-default-skin vjs-show-big-play-button-on-pause vjs-big-play-centered"
                                style={style}
                                controls/>
                        </div>
                }
            </div>
        )
    }
}