import React from "react";
import Navbar from "../Navbar";
import Hero from "./Hero";
import "../../stylesheets/faq-page.css";
import { ChakraProvider } from "@chakra-ui/react";

import QandA from "./QandA";
function FaqPage() {
  return (
    <ChakraProvider>
      <div>
        <section>
          <Navbar />
        </section>
        <section>
          <Hero />
        </section>
        <section>
          <QandA />
        </section>
      </div>
    </ChakraProvider>
  );
}

export default FaqPage;
