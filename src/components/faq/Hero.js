import React from "react";
// import "../../stylesheets/faq-page.css";
import "../../stylesheets/tailwindcss/tailwind.min.css";
function Hero() {
  return (
    <div className="bg-black -mt-12 py-6" style={{ height: "40vh" }}>
      <div className="flex justify-center w-full  pt-48">
        <h2 className="text-6xl font-semibold h-16">
          what do you need help with ?
        </h2>
      </div>
      <div className="mb-3 pt-0 w-1/4 mx-auto text-lg my-4 py-1">
        <input
          type="text"
          style={{ fontSize: "1.5rem" }}
          placeholder="type a question here"
          className="px-3 py-4 placeholder-gray-400 text-gray-700 relative bg-white bg-white rounded text-lg shadow outline-none focus:outline-none focus:shadow-outline w-full"
        />
      </div>
    </div>
  );
}

export default Hero;
