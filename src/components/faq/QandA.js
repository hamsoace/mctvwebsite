import React from "react";
import {
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  Box,
} from "@chakra-ui/react";
function QandA() {
  return (
    <div
      className="text-gray-700 container py-8 text-lg px-48"
      style={{ fontSize: "1.5rem" }}
    >
      <div>
        <h3 className="font-semibold text-5xl text-gray-800 ml-16">
          Porpular topics
        </h3>
      </div>
      <Accordion defaultIndex={[0]} allowMultiple>
        <AccordionItem margin="5rem">
          <AccordionButton
            fontSize="2.8rem"
            _expanded={{ bg: "black", color: "yellow.400" }}
          >
            <Box flex="1" textAlign="left" fontSize="2.8rem">
              How do i sign up for an account ?
            </Box>
            <AccordionIcon />
          </AccordionButton>
          <AccordionPanel pb={4}>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit
              amet consectetur adipisicing elit. Reiciendis, pariatur doloribus?
              Dolores voluptate, rem repellendus dolorem magnam magni nulla
              mollitia iure obcaecati? Qui et quas excepturi animi possimus,
              facilis dolorem ab ex perferendis voluptatum provident, dolor
              obcaecati eius quos esse fugit sed officiis natus, libero saepe?
              Nulla ut nihil amet. Lorem ipsum, dolor sit amet consectetur
              adipisicing elit. Repellendus unde dolorum, incidunt accusantium
              nemo omnis quo fuga aliquam provident corrupti veniam animi nihil
              sint reiciendis aliquid doloribus sed cum dolore. Veritatis
              nesciunt quos sapiente natus debitis pariatur distinctio
              laboriosam ullam corporis illo earum similique odit, suscipit
              facere quas quisquam? Nam!
            </p>
          </AccordionPanel>
        </AccordionItem>

        <AccordionItem margin="5rem">
          <AccordionButton
            fontSize="2.8rem"
            _expanded={{ bg: "black", color: "yellow.400" }}
          >
            <Box flex="1" textAlign="left" fontSize="2.8rem">
              {/* <h2 className="text-gray-800 text-4xl font-semibold"> */}
              How can i upgrade to a different plan ?{/* </h2> */}
            </Box>
            <AccordionIcon />
          </AccordionButton>
          <AccordionPanel pb={4}>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit
              amet consectetur adipisicing elit. Reiciendis, pariatur doloribus?
              Dolores voluptate, rem repellendus dolorem magnam magni nulla
              mollitia iure obcaecati? Qui et quas excepturi animi possimus,
              facilis dolorem ab ex perferendis voluptatum provident, dolor
              obcaecati eius quos esse fugit sed officiis natus, libero saepe?
              Nulla ut nihil amet. Lorem ipsum, dolor sit amet consectetur
              adipisicing elit. Repellendus unde dolorum, incidunt accusantium
              nemo omnis quo fuga aliquam provident corrupti veniam animi nihil
              sint reiciendis aliquid doloribus sed cum dolore. Veritatis
              nesciunt quos sapiente natus debitis pariatur distinctio
              laboriosam ullam corporis illo earum similique odit, suscipit
              facere quas quisquam? Nam!
            </p>
          </AccordionPanel>
        </AccordionItem>
        <AccordionItem margin="5rem">
          <AccordionButton
            fontSize="2.8rem"
            _expanded={{ bg: "black", color: "yellow.400" }}
          >
            <Box flex="1" textAlign="left" fontSize="2.8rem">
              {/* <h2 className="text-gray-800 text-4xl font-semibold"> */}
              Do you guys accept credit cards ?{/* </h2> */}
            </Box>
            <AccordionIcon />
          </AccordionButton>
          <AccordionPanel pb={4}>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit
              amet consectetur adipisicing elit. Reiciendis, pariatur doloribus?
              Dolores voluptate, rem repellendus dolorem magnam magni nulla
              mollitia iure obcaecati? Qui et quas excepturi animi possimus,
              facilis dolorem ab ex perferendis voluptatum provident, dolor
              obcaecati eius quos esse fugit sed officiis natus, libero saepe?
              Nulla ut nihil amet. Lorem ipsum, dolor sit amet consectetur
              adipisicing elit. Repellendus unde dolorum, incidunt accusantium
              nemo omnis quo fuga aliquam provident corrupti veniam animi nihil
              sint reiciendis aliquid doloribus sed cum dolore. Veritatis
              nesciunt quos sapiente natus debitis pariatur distinctio
              laboriosam ullam corporis illo earum similique odit, suscipit
              facere quas quisquam? Nam!
            </p>
          </AccordionPanel>
        </AccordionItem>
        <AccordionItem margin="5rem">
          <AccordionButton
            fontSize="2.8rem"
            _expanded={{ bg: "black", color: "yellow.400" }}
          >
            <Box flex="1" textAlign="left" fontSize="3rem">
              {/* <h2 className="text-gray-800 text-4xl font-semibold"> */}
              Supported Devices
              {/* </h2> */}
            </Box>
            <AccordionIcon />
          </AccordionButton>
          <AccordionPanel pb={4}>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit
              amet consectetur adipisicing elit. Reiciendis, pariatur doloribus?
              Dolores voluptate, rem repellendus dolorem magnam magni nulla
              mollitia iure obcaecati? Qui et quas excepturi animi possimus,
              facilis dolorem ab ex perferendis voluptatum provident, dolor
              obcaecati eius quos esse fugit sed officiis natus, libero saepe?
              Nulla ut nihil amet. Lorem ipsum, dolor sit amet consectetur
              adipisicing elit. Repellendus unde dolorum, incidunt accusantium
              nemo omnis quo fuga aliquam provident corrupti veniam animi nihil
              sint reiciendis aliquid doloribus sed cum dolore. Veritatis
              nesciunt quos sapiente natus debitis pariatur distinctio
              laboriosam ullam corporis illo earum similique odit, suscipit
              facere quas quisquam? Nam!
            </p>
          </AccordionPanel>
        </AccordionItem>
        <AccordionItem margin="5rem">
          <AccordionButton
            fontSize="2.8rem"
            _expanded={{ bg: "black", color: "yellow.400" }}
          >
            <Box flex="1" textAlign="left" fontSize="3rem">
              {/* <h2 className="text-gray-800 text-4xl font-semibold"> */}
              How do I download the Android app?
              {/* </h2> */}
            </Box>
            <AccordionIcon />
          </AccordionButton>
          <AccordionPanel pb={4}>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit
              amet consectetur adipisicing elit. Reiciendis, pariatur doloribus?
              Dolores voluptate, rem repellendus dolorem magnam magni nulla
              mollitia iure obcaecati? Qui et quas excepturi animi possimus,
              facilis dolorem ab ex perferendis voluptatum provident, dolor
              obcaecati eius quos esse fugit sed officiis natus, libero saepe?
              Nulla ut nihil amet. Lorem ipsum, dolor sit amet consectetur
              adipisicing elit. Repellendus unde dolorum, incidunt accusantium
              nemo omnis quo fuga aliquam provident corrupti veniam animi nihil
              sint reiciendis aliquid doloribus sed cum dolore. Veritatis
              nesciunt quos sapiente natus debitis pariatur distinctio
              laboriosam ullam corporis illo earum similique odit, suscipit
              facere quas quisquam? Nam!
            </p>
          </AccordionPanel>
        </AccordionItem>
        <AccordionItem margin="5rem">
          <AccordionButton
            fontSize="2.8rem"
            _expanded={{ bg: "black", color: "yellow.400" }}
          >
            <Box flex="1" textAlign="left" fontSize="2.8rem">
              {/* <h2 className="text-gray-800 text-4xl font-semibold"> */}
              How do I restart my account?
              {/* </h2> */}
            </Box>
            <AccordionIcon />
          </AccordionButton>
          <AccordionPanel pb={4}>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit
              amet consectetur adipisicing elit. Reiciendis, pariatur doloribus?
              Dolores voluptate, rem repellendus dolorem magnam magni nulla
              mollitia iure obcaecati? Qui et quas excepturi animi possimus,
              facilis dolorem ab ex perferendis voluptatum provident, dolor
              obcaecati eius quos esse fugit sed officiis natus, libero saepe?
              Nulla ut nihil amet. Lorem ipsum, dolor sit amet consectetur
              adipisicing elit. Repellendus unde dolorum, incidunt accusantium
              nemo omnis quo fuga aliquam provident corrupti veniam animi nihil
              sint reiciendis aliquid doloribus sed cum dolore. Veritatis
              nesciunt quos sapiente natus debitis pariatur distinctio
              laboriosam ullam corporis illo earum similique odit, suscipit
              facere quas quisquam? Nam!
            </p>
          </AccordionPanel>
        </AccordionItem>
        <AccordionItem margin="5rem">
          <AccordionButton
            fontSize="2.8rem"
            _expanded={{ bg: "black", color: "yellow.400" }}
          >
            <Box flex="1" textAlign="left" fontSize="2.8rem">
              {/* <h2 className="text-gray-800 text-4xl font-semibold"> */}I
              forgot my login credentials
              {/* </h2> */}
            </Box>
            <AccordionIcon />
          </AccordionButton>
          <AccordionPanel pb={4}>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit
              amet consectetur adipisicing elit. Reiciendis, pariatur doloribus?
              Dolores voluptate, rem repellendus dolorem magnam magni nulla
              mollitia iure obcaecati? Qui et quas excepturi animi possimus,
              facilis dolorem ab ex perferendis voluptatum provident, dolor
              obcaecati eius quos esse fugit sed officiis natus, libero saepe?
              Nulla ut nihil amet. Lorem ipsum, dolor sit amet consectetur
              adipisicing elit. Repellendus unde dolorum, incidunt accusantium
              nemo omnis quo fuga aliquam provident corrupti veniam animi nihil
              sint reiciendis aliquid doloribus sed cum dolore. Veritatis
              nesciunt quos sapiente natus debitis pariatur distinctio
              laboriosam ullam corporis illo earum similique odit, suscipit
              facere quas quisquam? Nam!
            </p>
          </AccordionPanel>
        </AccordionItem>
      </Accordion>
    </div>
  );
}

export default QandA;
