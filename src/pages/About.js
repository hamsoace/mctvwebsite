import React, { Component } from 'react'

import Page from '../components/Page'

class About extends Component {

    render() {
        return (
            <Page
                pageTitle="About MyMoviesAfrica™"
                showPageTitle={true}>
                <p>
                    MyMoviesAfrica&trade; just loves Movies.
                </p>

                <p>
                    So, we are extending Cinema (and soon we’ll be Substituting it - Where it
                    Doesn’t Exist) to Improve the Retail of Movies throughout Africa, as well
                    as the Retail of African Movies to our Diaspora.
                </p>

                <p>
                    MyMoviesAfrica&trade; achieves this via a Proprietary and proudly Kenyan mobile-first
                    Video On Demand (VOD) Service, that is Bringing Cinema Home, by Specialising in an
                    offering of Movies (i.e. Blockbusters, Features, Shorts, Documentaries, Animations, etc)
                    to Customers via an Android App on Phones, Tablets & Set-Top Boxes, via Chromecast on
                    Televisions and via Web on Laptops, Smart Televisions & other Smart Devices.
                </p>

                <div className="row">
                    <div className="col-sm-6">
                        <h4>Our Vision is:</h4>
                        <p>
                            An Entertainment Industry in Africa and its Diaspora, where Customers can conveniently Access
                            the Content of their Choice; and Creators can Access a Global Audience.
                        </p>
                    </div>
                    <div className="col-sm-6">
                        <h4>Our Mission is: </h4>
                        <p>
                            To conveniently Connect each Customer globally, with the Content (that Entertains, Inspires & Educates)
                            of their Choice; whilst equitably Remunerating each Rights Holder from Africa and the World, for
                            their continued Investment in Content Creation.
                        </p>
                    </div>
                </div>



                <h4>Our Objectives:</h4>

                <ol className="infolist">
                    <li>
                        Build longterm Relationships with our Customers and Clients, by providing exceptional Value in
                        Content (that Inspires, Educates & Entertains) and User Experience, via continuous Engagement
                        and Innovation.
                    </li>
                    <li>
                        Retain Customers and expand Recruitment, by offering Content based on their Preferences;
                        thereby, Living up to our Brand Promise of My Choice: Anything (variety), Anywhere (accessibility),
                        Anytime (convenience) – Personalised to each Customer, via their Viewing Habits and Feedback, as
                        well as deep Social Media Integration.
                    </li>
                    <li>
                        Equitably Monetise local Content, in addition to international, for the Benefit of all Rights Holders.
                    </li>
                    <li>
                        Work with all Stakeholders, for the Protection of Intellectual Property, and Eradication of Piracy.
                    </li>
                </ol>

                <h4>Credits</h4>
                <ul>
                    <li><a href="https://www.freepik.com/free-photos-vectors/background">www.freepik.com</a></li>
                </ul>
            </Page>
        );
    }
}

export default About