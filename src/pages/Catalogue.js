import React, { Component } from 'react'

import Page from '../components/Page'
import ContentListing from '../components/ContentListing'

export default class Catalogue extends Component{

	constructor(props){
		super(props)

		this.state = {
			results: []
		};
	}

	render(){

		return(
			<Page>
				<ContentListing />
			</Page>
		);
	}
}