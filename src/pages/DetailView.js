import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import Page from '../components/Page'
import CommentsBox from '../components/CommentsBox'
import RatingEmojis from '../components/RatingEmojis'
import Recommendations from '../components/Recommendations'

// code for sharing on social media
import {FacebookShareButton, TwitterShareButton, WhatsappShareButton, FacebookIcon, TwitterIcon, WhatsappIcon} from "react-share";
import { Helmet } from "react-helmet";

import { getArtwork } from '../utils/content'
import { getUserDetails, isLoggedIn } from '../utils/user-utils'

import { fetchContentInfo } from '../actions/content'
import { fetchUserPurchases, topupAccount, initBuyContent } from '../actions/payments'
import { fetchWallet, toggleUserFavourites } from '../actions/user'

class DetailView extends Component {

    constructor(props) {
        super(props)

        this.state = {
            ref: this.props.match.params.id,
            playTrailer: false,
            buyObj: {
                message:'',
                type: '',
                price: 0
            },
            buyComplete: false,
            user: getUserDetails()
        }
    }

    UNSAFE_componentWillReceiveProps = (nextProps) => {
        const { buyObj, buyComplete } = this.state
        // const { accountInfo } = nextProps.user
        const { buy={} } = nextProps.payments

        if(!buyComplete){
            if (buy.purchaseid) {
                this.setState({ buyComplete: true })
            }

            this.setState({
                buyObj: {
                    ...buyObj,
                    message: buy.message
                }
            })
        }
    }

    componentDidMount() {
        this.setState({ buyComplete: false })

        const { content } = this.props
        const { ref, user } = this.state

        if (ref && !content.contentInfo.hasOwnProperty('narrative')) {
            this.props.fetchContentInfo(ref, user.id)
        }

        if (isLoggedIn()) {
            const userObj = { user_id: user.id }

            this.props.fetchUserPurchases(userObj)
            this.props.fetchWallet(userObj)
        }
    }

    isPurchased = (contentRef) => {
        const {purchases} = this.props.payments

        return purchases.filter((p, i)=>{
            return p.ref === contentRef
        }).length > 0
    }

    initBuyContent(buyObj){
        this.setState({ buyObj })
    }

    buyContent = () => {
        let buyObj = this.state.buyObj

        const { content, user } = this.props
        const { ref } = content.contentInfo
        const { id, phone } = this.state.user

        const amount = buyObj.price

        if(!isLoggedIn()){
            buyObj.message = 'Log in to Rent or Own'
        }
        else if(this.isPurchased(ref) === 0){
            buyObj.message = 'This item has already been purchased'
        }
        else if(user.wallet.balance < amount){
            buyObj.message = 'You do not have enough money in your wallet. Top-up to continue'
            buyObj.topup = true
        }
        else if(!phone){
            buyObj.message = 'Please update your phone number to top-up your wallet'
        }
        else{
            
            // Proceed to buy content
            const purchaseObj = {
                user_id: id,
                ref,
                purchase_type: buyObj.type,
                source: 1
            }

            this.props.initBuyContent(purchaseObj)
        }

        this.setState({buyObj})
    }

    toggleTrailer = () => {
        this.setState({
            playTrailer: !this.state.playTrailer
        })
    }

    toggleFavourites = () => {
        const { user, ref } = this.state
        const obj = { user_id:user.id, ref }

        this.props.toggleUserFavourites(obj)
    }

    render() {
        // console.log(this.props.payments, this.state)

        const { buyObj, buyComplete } = this.state
        const { content, payments } = this.props

        let {
            ref,
            synopsis,
            classification,
            title,
            year,
            rating,
            genres,
            duration,
            tags,
            trailer_url,
            fav,

            currency,
            est_price,
            rental_price,

            error,
            message

        } = content.contentInfo

        // est_price = Math.round(est_price)
        // rental_price = Math.round(rental_price)

        const imgUrl = ref ? getArtwork(ref).portrait : ''

        return (
            <Page 
                pageTitle={title}>

            {
                error ?
                    <div className="alert alert-danger">{message}</div>
                :
                <div className="row detail-view-holder">
                    <div className="col-sm-3">
                        <img src={imgUrl} alt={title} className="img-responsive" />
                    </div>

                    <div className="col-sm-9 col-md-8 content-info">
                        <h1 style={{ marginTop: 0 }}>{title}</h1>
                        
                        {
                            !content.contentInfoLoading ?

                                <div>
                                    <p className="content-tags">
                                        <span><i className="icon ion-md-calendar"></i> {year}</span>
                                        <span><i className="icon ion-md-clock"></i> {`${duration} minutes`}</span>
                                        <span><i className="icon ion-md-paper"></i> {`${classification}`}</span>
                                        <br/>
                                        <span><i className="icon ion-md-pricetags"></i> {`${tags}`}</span>
                                    </p>

                                    <div className="content-rating-emojis">
                                        Rating: &nbsp;
                                        <RatingEmojis rating={rating} />
                                    </div>
                                    <hr />

                                    <div
                                        id="read-more"
                                        className="content-synopsis"
                                        dangerouslySetInnerHTML={{ __html: synopsis }}></div>

                                    <hr />
                                </div>
                            : null
                        }

                        {isLoggedIn() ?

                        <div className="content-buttons">

                            {!payments.purchasesLoading ?
                                <span>
                                {this.isPurchased(ref) || buyComplete ?
                                    <NavLink to={"/playback/" + ref} className="btn btn-success">
                                        <i className="icon ion-md-play"></i> <strong>Watch Now</strong>
                                    </NavLink>
                                    :
                                    <span>
                                        {
                                            est_price != '1' ?
                                                <button
                                                    className="btn btn-primary"
                                                    data-toggle="modal" 
                                                    data-target="#pmodal"
                                                    onClick={() => {
                                                        this.initBuyContent({ type: 'EST', price: est_price })
                                                    }}>

                                                    <i className="icon ion-md-cart"></i> &nbsp;
                                                    <small>Own</small>
                                                    <strong>{`${currency} ${est_price}`}</strong>
                                                </button>
                                            : null
                                        }

                                        {
                                            //rental_price != '1' ?
                                                <button
                                                    className="btn btn-success"
                                                    data-toggle="modal"
                                                    data-target="#pmodal"
                                                    onClick={() => {
                                                        this.initBuyContent({ type: 'RENTAL', price: rental_price })
                                                    }}>
                                                    
                                                    <i className="icon ion-md-cart"></i> &nbsp;
                                                    <small>Rent 48hrs </small>
                                                    <strong>{`${currency} ${rental_price}`}</strong>
                                                </button>
                                            //: null
                                        }
                                    </span>
                                }

                                    
                                </span>
                                : null 
                            }

                            <button
                                className="btn btn-secondary"
                                onClick={this.toggleTrailer}>

                                <i className="icon ion-md-film"></i> <strong>Trailer</strong>
                            </button>

                            <button
                                className={`btn btn-secondary btn-favourite ${fav ? 'is-favourite' : ''}`}
                                onClick={this.toggleFavourites}>

                                <i className="icon ion-md-heart"></i> &nbsp;
                                <strong>{fav ? 'Remove' : 'Add'}</strong>
                            </button>

                            {/* sharing to social media button */}
                            <Helmet>
                                            <meta property="og:title" content={title} />
                                            <meta property="og:hashtag" content="#OwnForLife" />
                                            <meta property="og:image" content={imgUrl} />
                                            <meta property="og:image:width" content="500" />
                                            <meta property="og:image:height" content="500" />
                                            <meta content="image/*" property="og:image:type" />
                                            <meta property="og:url" content={"https://mymovies.africa/view/" + `${ref}`} />
                                            <meta property="og:site_name" content="MyMoviesAfrica" />
                                            <meta property="og:description" content={synopsis} />
                                            <iframe style="width: 0px; height: 0px; margin: 0px; padding: 0px;" src={imgUrl}></iframe>

                                            <meta name="twitter:title" content={title} />
                                            <meta name="twitter:description" content={synopsis} />
                                            <meta name="twitter:image" content={imgUrl} />
                                            <meta name="twitter:card" content="summary_large_image" />
                                        </Helmet>

                                        <FacebookShareButton
                                            url={"https://mymovies.africa/view/" + `${ref}`}
                                            quote={"Currently watching " + `${title}` + " on MyMoviesAfrica. Check it out!"}
                                            hashtag=""
                                            className={`btn social-icons`}>
                                            <FacebookIcon size={38} />
                                        </FacebookShareButton>

                                        <TwitterShareButton
                                            url={"Currently watching " + `${title}` + " on MyMovies.africa Check it out!  #OwnForLife #MyMoviesAfrica " + `#${title}`}
                                            hashtag="OwnForLife"
                                            className={`btn social-icons`}>
                                            <TwitterIcon size={38} />
                                        </TwitterShareButton>
                        </div>
                        :
                        <div>
                            <button
                                className="btn btn-secondary"
                                onClick={this.toggleTrailer}>

                                <i className="icon ion-md-film"></i> <strong>Trailer</strong>
                            </button>
                            <NavLink to="/login" className="btn btn-lg btn-success">
                                <i className="icon ion-md-person"></i> &nbsp; Log in to Rent or Own
                            </NavLink>
                        </div>
                        }
                    </div>

                    <div className="col-sm-12">
                        <hr />
                        
                        {/* ref ? <Recommendations contentRef={ref} genres={genres} /> : null */}

                        <div className="row">
                            <div className="col-md-6">
                                <CommentsBox />
                            </div>
                        </div>
                    </div>

                    {
                        this.state.playTrailer ?
                            <div className="ytTrailer">
                                <div className="trailer-overlay"></div>
                                <div className="trailer-modal">
                                    <button className="btn btn-danger" onClick={this.toggleTrailer}>
                                        &times;
                                    </button>
                                    <iframe
                                        title="ytIframe"
                                        src={"https://www.youtube.com/embed/" + trailer_url}
                                        frameBorder="0"
                                        allowFullScreen></iframe>
                                </div>
                            </div>
                            : null
                    }

                    <div className="modal" id="pmodal"
                        tabIndex="-1" role="dialog" aria-hidden="true">
                        <div className="modal-dialog">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 className="modal-title">{ buyObj.type == 'RENTAL' ? `Rent ${title}` : `Own ${title}` }</h4>
                                </div>
                                <div className="modal-body clearfix">

                                    <div className="row">
                                        <div className="col-sm-3">
                                            <img src={imgUrl} alt={title} className="img-responsive" />
                                        </div>

                                        {isLoggedIn? 
                                        <div className="col-sm-9">
                                            <h4>{title}</h4>
                                            <p>
                                                Transaction Type: <span className="text-danger">{ buyObj.type.toUpperCase() == 'RENTAL' ? 'RENTAL' : 'OWN' }</span>
                                            </p>
                                            
                                            <h5>
                                                {`${currency}. ${buyObj.price}` }
                                            </h5>
                                            <br/>

                                            <div className="form-group">
                                                {
                                                    buyObj.message ?
                                                        <div>
                                                            {buyObj.message}

                                                            <div>
                                                            <br/>
                                                            {
                                                                buyObj.topup ?
                                                                <button 
                                                                    onClick={()=>{
                                                                        window.location.href = '/my-account'
                                                                    }} 
                                                                    className="btn btn-primary">Top-up Now</button> 
                                                                : null
                                                            }
                                                            
                                                                <button className="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>

                                                        </div>
                                                    :  <div>
                                                            <p>
                                                                Almost there! Ready the Popcorn!! After you click { buyObj.type == 'RENTAL' ? 'Rent Now' : 'Own Now' }, click WATCH NOW to Enjoy your Movie! <span role="img" aria-label="Smiley">😊</span>
                                                            </p>
                                                            <br/>
                                                            <button className="btn btn-success" onClick={this.buyContent}>{ buyObj.type == 'RENTAL' ? 'Rent Now' : 'Own Now' }</button>
                                                            <button className="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                }
                                                
                                            </div>
                                        </div>
                                        : 
                                        <div className="alert alert-info">
                                            Log in to Rent or Own
                                        </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
            </Page>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        content: state.content,
        payments: state.payments,
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        fetchContentInfo,
        fetchUserPurchases,
        fetchWallet,
        topupAccount,
        initBuyContent,

        toggleUserFavourites
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailView)
