import React, { Component } from 'react'

import Page from '../components/Page'
import androidPhone from '../images/download/android-phone.png'
import googlePlayBadge from '../images/download/google-play-badge.png'

export default class DownloadApp extends Component {
    
    render() {
        return (
            <Page>
                <div className="page-container download-app">
                    <div className="col-xs-12 col-sm-7 app-info">
                        <h1 className="download-header">Download the MyMoviesAfrica App</h1>
                        <p>
                            <i className="icon ion-md-checkmark"></i> &nbsp;
                            Download and watch movies from anywhere
                        </p>
                        <p>
                            <i className="icon ion-md-checkmark"></i>  &nbsp;
                            Membership is Free! No Subscription!! Pay only for the Movies that you Rent or #OwnForLife
                        </p>

                        <a 
                            href="https://play.google.com/store/apps/details?id=org.mychoicetv.app"
                            target="_blank"
                            rel="noopener noreferrer">
                            <img src={ googlePlayBadge } alt="Google Play"
                                className="img-responsive google-play-badge"/>
                        </a>

                        <p>
                            
                            &nbsp;

                        </p>

                        <p>
                            Or Click 
                            
                            <a 
                            href="https://dev.mymovies.africa/mymoviesafrica_2.3.3.apk"
                            target="_blank"
                            rel="noopener noreferrer">
                            &nbsp;
                            <b>Here</b>
                            &nbsp;
                            </a>

                            To Download the App directly from our Website.
                        </p>

                    </div>
                    <div className="col-xs-12 col-sm-5 app-info">
                        <img src={androidPhone} alt="MyMoviesAfrica App" 
                            className="img-responsive app-screenshot"/>
                    </div>
                </div>
            </Page>
        )
    }
}