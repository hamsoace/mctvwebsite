import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { Carousel } from 'react-responsive-carousel'
import 'react-responsive-carousel/lib/styles/carousel.min.css'

import Page from '../components/Page'
import ContentListing from '../components/ContentListing'

import URL from '../utils/urls'

class Home extends Component {

    constructor(props) {
        super(props)

        this.state = {}

        if (localStorage.getItem("displayPrompt") == null) {
            localStorage.setItem("displayPrompt", false)
            // display an alert to the user regarding our preferred browsers
            alert("For the best Viewing experience, we Recommend using Google Chrome, Firefox for Windows, Android, and MacOS on Phones, Tablets and Laptops. MyMoviesAfrica™ is currently only Compatible with these Browsers, Operating Systems and Devices. However, MyMoviesAfrica™ will soon be Available on iOS devices, Smart Televisions and Set Top Boxes. Meanwhile, we always welcome Feedback from our Customers via Jivo Chat, Facebook, Instagram, Twitter, LinkedIn or info@mymovies.africa. Meanwhile, Bring Cinema Home for #TheGreatNightIn.")
        }
    }

    componentDidMount() { }

    render() {
        const { banners=[], content=[] } = this.props.content.contentList

        return (
            <Page
                pageTitle="MyMoviesAfrica™ - Rent a Movie online or #OwnForLife!"
                preserveScroll={true}>

                <div className="col-sm-12 home-carousel">
                    {
                        banners.length > 0 ?

                            <Carousel 
                                autoPlay={true}
                                infiniteLoop={false}
                                showArrows={false}
                                showStatus={false}
                                useKeyboardArrows={true}>

                                {
                                    banners.map((b, i) => {

                                        return (
                                            <div className="clearfix" key={i}>
                                                <NavLink to={`/view/${b.ref}`}>
                                                    <div className="row">
                                                        <div className="col-md-8 banner-img">
                                                            <img
                                                                src={`${URL.banners}${b.image}`}
                                                                alt={b.description}
                                                                className="img-responsive" />
                                                        </div>
                                                        <div className="col-md-4 banner-desc hidden-sm">
                                                            <h2>{b.title}</h2>
                                                            <p>{b.description}</p>
                                                        </div>
                                                    </div>
                                                </NavLink>
                                            </div>
                                        )
                                    })
                                }
                            </Carousel>
                        :
                        null
                    }
                </div>

                <div className="clearfix">
                    {
                        content.message ?
                            <div className="alert alert-info">{content.message}</div>
                            :
                            <ContentListing />
                    }
                </div>
            </Page>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        content: state.content
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)