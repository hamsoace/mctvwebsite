import React, { Component } from 'react';

import Page from '../components/Page';
import { addRequest } from "../utils/api";
import Swal from "sweetalert2";


class RequestMovie extends Component{
    constructor(props) {
        super(props)
    
        this.state = {
          email:'',
          movie:''  
        }
    }
    handleEmailChange = (event) => {
        this.setState({
            email: event.target.value,
            isSubmitted:false
        })
    }
    handleMovieChange = (event) => {
        this.setState({
            movie: event.target.value
        })
    }
    handleSubmit = (event) => {
        event.preventDefault();
        
        const obj = {
            email: this.state.email,
            movie: this.state.movie,
          }
          console.log(obj)
          addRequest(obj)
          .then(res => {
            console.log(res);
            console.log(res.data);
              Swal.fire({
                  title: 'Success',
                  text: `Your movie request was successful!`,
                  icon: 'success',
                  confirmButtonText: 'Close'
              })
        }).catch(err => {
            console.log(err)
        });
        this.setState({email: ""});
        this.setState({movie: ""});

    }
    
    render(){
        const{email, movie} = this.state
		return(
            <Page pageTitle="Movie Request">
                <h4>Didn't find the movie you wish to watch?
                     Just make a request and be sure to get it next time.</h4>
                
                <form className="col-md-6"onSubmit={this.handleSubmit} method='POST'>
                <div className="form-group">
               
                   <label>Email:</label> 
                   <input type='email'
                   name="email"
                   className='form-control'
                   value={email} 
                   onChange={this.handleEmailChange}
                   required />
               
               </div>

                <div className="form-group">
               
                   <label>Movie Name:</label> 
                   <input type='text'
                   name="movie"
                   className='form-control'
                   value={movie} 
                   onChange={this.handleMovieChange}
                   required />
               
               </div>
               <div className="form-group">
                    <button className="btn btn-success">Request</button>
                </div>
                </form>

            </Page>
        );
    }
}
    export default RequestMovie;