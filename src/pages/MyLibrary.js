import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'

import Page from '../components/Page'
import Carousel from '../components/Carousel'
import ContentCard from '../components/ContentCard'

import { getUserDetails } from '../utils/user-utils'
import { fetchBundle } from '../actions/user'

class MyLibrary extends Component{

	constructor(props){
		super(props)

		this.state = {
			user: getUserDetails()
		}
    }
    
    componentDidMount = () => {
        const {user} = this.state

        if (this.state.user.hasOwnProperty('id')) {
            this.props.fetchBundle({ user_id: user.id })
        }
    }
    
	render(){
        // console.log(this.props)

        const { bundle={}, bundleLoading } = this.props.user
        const {views, favourites} = bundle

		return(
			<Page pageTitle="My Library">
				
                {
                    !bundleLoading && views.length === 0 && favourites.length === 0 ?

                        <div className="blank-state clearfix">
                            <span>Pretty lonely in here. We recommend viewing our </span>
                            <NavLink to="/catalogue">Catalogue</NavLink>
                        </div>
                    :
                        <div className="clearfix">
                            {
                                views.length > 0 ?

                                <div className="clearfix">
                                    <h4 className="col-sm-12">Continue Watching</h4>

                                    <Carousel
                                        arrows={true}
                                        infinite={false}
                                        slidesToShow={6}
                                        slidesToScroll={3}
                                        responsive={
                                            [
                                                {
                                                    breakpoint: 768,
                                                    settings: {
                                                        slidesToShow: 4,
                                                        slidesToScroll: 4
                                                    }
                                                },
                                                {
                                                    breakpoint: 480,
                                                    settings: {
                                                        slidesToShow: 2,
                                                        slidesToScroll: 2
                                                    }
                                                }
                                            ]
                                        }>

                                        {
                                            views.map((view, i) => {

                                                return (
                                                    <div key={i}>
                                                        <ContentCard
                                                            contentInfo={view}
                                                            className="resume-holder"
                                                            hasPlayButton={true}
                                                            key={i} />
                                                    </div>
                                                )
                                            })
                                        }
                                    </Carousel>
                                </div>

                                : null
                            }

                            {
                                favourites.length > 0 ?

                                    <div>
                                        <h4 className="col-sm-12">Favourites</h4>

                                        <Carousel
                                            arrows={true}
                                            infinite={false}
                                            slidesToShow={6}
                                            slidesToScroll={3}
                                            responsive={
                                                [
                                                    {
                                                        breakpoint: 768,
                                                        settings: {
                                                            slidesToShow: 4,
                                                            slidesToScroll: 4
                                                        }
                                                    },
                                                    {
                                                        breakpoint: 480,
                                                        settings: {
                                                            slidesToShow: 2,
                                                            slidesToScroll: 2
                                                        }
                                                    }
                                                ]
                                            }>

                                            {
                                                favourites.map((view, i) => {
                                                    const ct = {
                                                        ref: view.ref,
                                                        title: view.title
                                                    }

                                                    return (
                                                        <div key={i}>
                                                            <ContentCard
                                                                contentInfo={ct}
                                                                key={i} />
                                                        </div>
                                                    )
                                                })
                                            }
                                        </Carousel>
                                    </div>

                                    : null
                            }
                        </div>
                }
			</Page>
		)
	}
}



const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        fetchBundle
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(MyLibrary)