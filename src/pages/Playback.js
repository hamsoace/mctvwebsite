import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import $ from 'jquery'

import Page from '../components/Page'
import Video from '../components/Video'
// import MediaPlayer from '../components/MediaPlayer'

import { fetchContentInfo } from '../actions/content'
import { fetchUserPurchases } from '../actions/payments'

import { getUserDetails, isLoggedIn } from '../utils/user-utils'

class Playback extends Component {

    constructor(props) {
        super(props)

        this.state = {
            contentRef: this.props.match.params.id,
            user: getUserDetails()
        }

        // Set login redirect
        if (this.props.requiresLogin !== 'undefined') {
            if (!isLoggedIn()) {
                this.props.history.push('/')
            }
        }

        // Display alert dialog with a prompt to guide the user
        // alert ("Your Movie is About to Begin! If it doesn't Start after 10 seconds, Move the Seeker a few Seconds into the Movie. Enjoy! 😊");
    }

    componentDidMount() {
        const { contentRef, user } = this.state

        if(contentRef){
            this.props.fetchContentInfo(contentRef, user.id)
        }

        if (isLoggedIn()) {
            const userObj = { user_id: user.id }

            this.props.fetchUserPurchases(userObj)
        }

        $('body').addClass('video-player')
    }

    componentWillUnmount(){
        $('body').removeClass('video-player')
    }

    isPurchased = (contentRef) => {
        const { purchases } = this.props.payments

        return purchases.filter((p, i) => {
            return p.ref === contentRef
        }).length > 0
    }

    render() {
        const {
            title, 
            ref=this.state.contentRef 
        } = this.props.content.contentInfo
        
        // TODO: Activate subtitles when feature is added in backend
        const subtitles = '';

        return (
            <Page 
                pageTitle={"Playing " + title} 
                fullScreen={true}
                requiresLogin={true}>

                <div className="clearfix">
                    <div className="media-player-bar">
                        <h2>
                            <button
                                className="btn btn-back-media-player"
                                onClick={() => {
                                    window.history.back()
                                }}>

                                <i className="icon ion-md-arrow-back"/>
                            </button>
                            Now Playing: {title}
                        </h2>
                    </div>

                    <Video 
                        contentRef={ref}
                        title={title}
                        subtitle={subtitles} />

                    {/* <MediaPlayer
                        contentId={'67895432987623454756654729382341'}
                        contentKey={'43210987123478904321098712340987'}
                        contentRef={ref}
                        contentTitle={title}
                        subtitleURL={subtitles}
                        hasExpiry={true} /> */}
                </div>
            </Page>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return { content: state.content, user: state.user }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        fetchContentInfo,
        fetchUserPurchases,
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Playback)