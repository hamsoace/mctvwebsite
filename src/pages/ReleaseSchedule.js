import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

import Page from '../components/Page'

export default class ReleaseSchedule extends Component {
    
    render() {
        return (
            <Page pageTitle="Release Schedule">
                <div className="page-container">
                    <table id="schedule-table" className="table table-striped">
                        <thead>
                            <tr className="head">
                                <th width="30%"> Title</th>
                                <th>Type</th>
                                <th>Genre</th>
                                <th>Release Date</th>
                                <th>Studio</th>
                                <th width="270px">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Suicide Squad 2D/IMAX 2D</td>
                                <td>Film</td>
                                <td>Superhero, Action</td>
                                <td>5th August, 2016</td>
                                <td>Warner</td>
                                <td>
                                    <NavLink to="/" className="btn btn-sm btn-warning">
                                        <i className="icon ion-md-play">Watch Trailer</i>
                                    </NavLink>
                                    <NavLink to="/" className="btn btn-sm btn-success">
                                        <i className="icon ion-md-play">Add to Wishlist</i>
                                    </NavLink>
                                </td>
                            </tr>
                            <tr>
                                <td>The Perfect Weapon</td>
                                <td>Film</td>
                                <td>Drama</td>
                                <td>12th August, 2016</td>
                                <td>Independent/Hindi</td>
                                <td>
                                    <NavLink to="/" className="btn btn-sm btn-warning">
                                        <i className="icon ion-md-play">Watch Trailer</i>
                                    </NavLink>
                                    <NavLink to="/" className="btn btn-sm btn-success">
                                        <i className="icon ion-md-play">Add to Wishlist</i>
                                    </NavLink>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </Page>
        )
    }
}