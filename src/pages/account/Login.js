import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'

import Swal from 'sweetalert2'

import Page from '../../components/Page'

import { setUserSession, isLoggedIn } from '../../utils/user-utils'
import { auth, googleAuthProvider, facebookAuthProvider } from '../../utils/firebaseAuth'

import { loginUser } from '../../actions/user'

import btnSigninGoogle from '../../images/btn_signin_google.png'
import btnSigninFacebook from '../../images/btn_signin_facebook.png'

class Login extends Component{
    
    constructor(props){
        super(props)

        this.state = {
            user: null,

            name: '',
            email: '',
            phone: '',
            password: '',

            register: {},
            login: {}
        }
    }

    componentDidMount = () => {

        if(isLoggedIn()){
            this.props.history.push('/')
            
        }
    }

    UNSAFE_componentWillReceiveProps = (nextProps) => {
        const { accountInfo={} } = nextProps.user

        if( accountInfo.hasOwnProperty('email') && accountInfo.hasOwnProperty('phoneNumber') && accountInfo.hasOwnProperty('displayName') ) {
            setUserSession(accountInfo)
            this.props.history.push('/')

        } 
        else {
            setUserSession(accountInfo)
            this.props.history.push('/my-account')
            Swal.fire({
                title: '',
                text: 'Please ensure you input your active mobile number.',
                icon: 'info',
                confirmButtonText: 'Close'
            })
        }
    }

    loginWithGoogle = (e) => {

        auth.signInWithPopup(googleAuthProvider) 
            .then((result) => {
                const { displayName, email, phoneNumber, photoURL } = result.user
                const userObj = {
                    name: displayName,
                    email,
                    phone_number: phoneNumber,
                    photo_url: photoURL,
                }

                if(result.user){
                    this.props.loginUser(userObj)

                }
            })
            .catch((error) => {
                Swal.fire({
                    title: 'Error',
                    text: 'Error signing in. Please try again',
                    icon: 'error',
                    confirmButtonText: 'Close'
                })
            })
    }

    loginWithFacebook = (e) => {

        auth.signInWithPopup(facebookAuthProvider)
            .then((result) => {
                console.log(result)

                const { displayName, email, phoneNumber, photoURL } = result.user
                const userObj = {
                    name: displayName,
                    email,
                    phone_number: phoneNumber,
                    photo_url: photoURL,
                }

                if (result.user) {
                    this.props.loginUser(userObj)
                }
            })
            .catch((error) => {
                console.log(error)

                if(error.code === 'auth/account-exists-with-different-credential'){
                    const userObj = { name: '.', email: error.email }

                    this.props.loginUser(userObj)
                }
                else{
                    Swal.fire({
                        title: 'Error',
                        text: 'Error signing in. Please try again',
                        icon: 'error',
                        confirmButtonText: 'Close'
                    })
                }
            })
    }

    onChange = (e) => {
        e.preventDefault()

        const { name, value } = e.target

        this.setState({
            [name]: value
        })
    }

    onRegister = (e) => {
        e.preventDefault()

        const { fname, lname, phone, email, password } = this.state

        if(password.length < 6){
            // alert('Password should be at least 6 characters long')
            Swal.fire({
                title: 'Error',
                text: 'Password should be at least 6 characters long',
                icon: 'error',
                confirmButtonText: 'Close'
            })
        }
        else{

            auth.createUserWithEmailAndPassword(email, password)
            .then((result) => {
                // console.log(result.user)

                const { email, photoURL } = result.user

                const userObj = {
                    name: `${fname} ${lname}`,
                    email,
                    phone_number: phone,
                    photo_url: photoURL,
                    password
                }

                if (result.user) {
                    this.props.loginUser(userObj)
                }
            })
            .catch((error) => {
                // console.log(error)

                if(error.code === 'auth/email-already-in-use'){
                    this.setState({
                        register: {
                            error: true,
                            message: 'This email address is already in use'
                        },
                        password: ''
                    })
                }
            })

            // Display alert dialog
            alert ('Thank you for joining MyMoviesAfrica™! You can update your active Mobile Number on My Account (Top Right). Also, if you Enter your Date of Birth, we Promise a Surprise on your Birthday. 😉');
        }
    }

    onLogin = (e) => {
        e.preventDefault()

        const { email, password } = this.state

        auth.signInWithEmailAndPassword(email, password)
            .then((result) => {
                console.log(result.user)
                const { displayName='_', email, phoneNumber, photoURL } = result.user

                const userObj = {
                    name: displayName,
                    email,
                    phone_number: phoneNumber,
                    photo_url: photoURL,
                    password
                }

                this.props.loginUser(userObj)
            })
            .catch((error) => {
                this.setState({
                    login: {
                        error: true,
                        message: error.message
                    },
                    password: ''
                })
            })
    }
    
    render(){
        const { fname, lname, email, phone, password, register, login } = this.state
        
        return(
            <Page pageTitle="Login">

                <div className="clearfix login-holder">
                    <h4>Sign-in to Enjoy the latest Movies available for 48-hour Rent or to #OwnForLife</h4>

                    <div className="clearfix">
                        <ul className="nav nav-tabs">
                            <li className="active">
                                <a href="#login" data-toggle="tab">LOGIN</a>
                            </li>
                            <li>
                                <a href="#register" data-toggle="tab">REGISTER</a>
                            </li>
                        </ul>
                    
                        <div className="tab-content">
                            <div className="tab-pane active" id="login">

                                {
                                    login.error ?
                                        <div className="alert alert-danger">{login.message}</div>
                                        : null
                                }

                                {
                                    !login.error && login.message ?
                                        <div className="alert alert-success">{login.message}</div>
                                        : null
                                }
                                <form
                                    className="form-horizontal"
                                    onSubmit={this.onLogin}>

                                    <div className="form-group">
                                        <input
                                            type="text"
                                            name="email"
                                            className="form-control"
                                            placeholder="Email Address"
                                            value={email}
                                            onChange={this.onChange}
                                            autoFocus
                                            required />
                                    </div>

                                    <div className="form-group">
                                        <input
                                            type="password"
                                            name="password"
                                            className="form-control"
                                            placeholder="Password"
                                            value={password}
                                            onChange={this.onChange}
                                            required />
                                    </div>

                                    <div className="form-group">
                                        <button className="btn btn-lg btn-block btn-warning">Login</button>
                                    </div>
                                </form>

                                <p>
                                    Forgot your password? <NavLink to="/reset-password">Click here to reset</NavLink>
                                </p>

                            </div>

                            <div className="tab-pane" id="register">
                                <form
                                    className="form-horizontal"
                                    onSubmit={this.onRegister}>

                                    {
                                        register.error ?
                                            <div className="alert alert-danger">{register.message}</div>
                                            : null
                                    }

                                    {
                                        !register.error && register.message ?
                                            <div className="alert alert-success">{register.message}</div>
                                            : null
                                    }

                                    <div className="form-group">
                                        <input
                                            type="text"
                                            name="fname"
                                            className="form-control"
                                            placeholder="First Name"
                                            defaultValue={fname}
                                            onChange={this.onChange}
                                            autoFocus
                                            required />
                                    </div>

                                    <div className="form-group">
                                        <input
                                            type="text"
                                            name="lname"
                                            className="form-control"
                                            placeholder="Last Name"
                                            defaultValue={lname}
                                            onChange={this.onChange}
                                            required />
                                    </div>

                                    <div className="form-group">
                                    <input
                                            className="form-control"
                                            name="phone"
                                            placeholder="Mobile Number"
                                            onChange={this.onChange}
                                            value={phone}
                                            required />
            
                                    </div>

                                    <div className="form-group">
                                        <input
                                            type="email"
                                            name="email"
                                            className="form-control"
                                            placeholder="Email Address"
                                            defaultValue={email}
                                            onChange={this.onChange}
                                            required />
                                    </div>


                                    <div className="form-group">
                                        <input
                                            type="password"
                                            name="password"
                                            className="form-control"
                                            placeholder="Password"
                                            defaultValue={password}
                                            onChange={this.onChange}
                                            required />
                                    </div>

                                    <div className="form-group">
                                        <button className="btn btn-lg btn-block btn-success">Create Account</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <hr/>

                    <div className="clearfix social-login">
                        <img
                            src={btnSigninGoogle}
                            alt="Sign in with Google"
                            className="btn-login"
                            onClick={this.loginWithGoogle} />

                        <img
                            src={btnSigninFacebook}
                            alt="Login with Facebook"
                            className="btn-login"
                            onClick={this.loginWithFacebook} />
                    </div>
                </div>
            </Page>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ loginUser }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)