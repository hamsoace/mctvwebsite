import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import 'react-phone-number-input/style.css'
import PhoneInput from 'react-phone-number-input'

import DatePicker from '../../components/DatePicker'
import Page from '../../components/Page'
import PaymentsModal from '../../components/PaymentsModal'
import ContentCard from '../../components/ContentCard'

import { processCheckIn } from '../../utils/api'
import { getUserDetails, setUserSession, isLoggedIn } from '../../utils/user-utils'
import { updateUser, fetchWallet, fetchBundle, toggleUserFavourites } from '../../actions/user'
import { fetchUserPayments } from '../../actions/payments'

import avatar from '../../images/default.png'

class MyAccount extends Component {

    constructor(props){
        super(props)

        const user = getUserDetails()
        const { fullname, email, phone, birthday } = user

        this.state = {
            user,
            bundle: {},
            updatedUser: {
                fullname,
                email,
                phone_number: phone,
                birthday
            },

            // For screenings
            screenings: [],
            checkIn: {}
        }
    }

    componentDidMount(){

        if(this.state.user.hasOwnProperty('id')){
            this.loadUserData()
        }

        if(!isLoggedIn()){
            this.props.history.push('/login')
        }
    }

    processCheckIn = (e) => {
        e.preventDefault()
        // console.log(e, this.refs.refCode)

        const checkIn = {
            refcode: this.refs.refCode.value,
            user_id: this.state.user.id
        }

        processCheckIn(checkIn)
            .then((response)=>{
                this.setState({ checkIn: response.data })
            })
    }

    UNSAFE_componentWillReceiveProps = (nextProps) => {
        const { accountInfo={}, message } = nextProps.user

        if (accountInfo.hasOwnProperty('email')) {
            setUserSession(accountInfo)

            this.setState({
                user: {
                    ...accountInfo
                },
                message
            })
        }
    }

    loadUserData = () => {
        const {
            //fetchUserPayments, 
            fetchWallet,
            fetchBundle
        } = this.props

        const {user} = this.state

        const userObj = { user_id: user.id }
    
        // fetchUserPayments(userObj)
        fetchWallet(userObj)
        fetchBundle(userObj)
    }

    updateProfile = (e) => {
        e.preventDefault()
        this.props.updateUser(this.state.updatedUser)
    }

    onDatePicked = (date) => {
        const { updatedUser } = this.state

        this.setState({
            updatedUser: {
                ...updatedUser,
                birthday: date
            }
        })
    }

    onChangePhone = (phone) => {

        if(phone){

            this.setState({
                updatedUser: {
                    ...this.state.updatedUser,
                    phone_number: phone
                }
            })
        }
    }

    onChange = (e) => {
        e.preventDefault()

        const { name, value } = e.target
        const { updatedUser } = this.state

        this.setState({
            updatedUser: {
                ...updatedUser,
                [name]: value
            }
        })
    }
    
    render() {
        // console.log(this.state.updatedUser)
        // console.log(this.props)

        const { user } = this.props
        const { bundle={}, wallet={} } = user

        const { checkIn } = this.state
        const { fullname, email, phone, photo_url, birthday, locale='KE' } = this.state.user

        const profileAvatar = photo_url && photo_url !== 'null' ? photo_url : avatar
        const profileName = fullname || ''

        return(
            <Page 
                pageTitle="My Account"
                preserveScroll={ true }>

                <div className="row">

                    <div className="col-sm-3">
                        <div className="clearfix profile-sidebar">
                            <div className="profile-img">
                                <img 
                                    src={ profileAvatar } 
                                    className="img-responsive" 
                                    alt={profileName} />

                                <h4>{ profileName }</h4>
                                <p>
                                    <em>Email: {email}</em><br/>
                                    <em>Phone: {phone || '-'}</em><br/>
                                </p>
                                <hr/>
                            </div>
                        </div>
                    </div>

                    <div className="col-sm-9">

                        <ul className="nav nav-tabs">
                            <li className="active"><a data-toggle="tab" href="#account">Account</a></li>
                            <li><a data-toggle="tab" href="#history">History</a></li>
                            <li><a data-toggle="tab" href="#favourites">Favourites</a></li>
                            <li><a data-toggle="tab" href="#downloads" className="hidden">Downloads</a></li>
                            <li><a data-toggle="tab" href="#devices" className="hidden">Devices</a></li>
                            <li><a data-toggle="tab" href="#purchases">MyMovies</a></li>
                            <li><a data-toggle="tab" href="#check-in">Check-In</a></li>
                        </ul>

                        <div className="tab-content">
                            <div id="account" className="tab-pane active">
                                <div className="col-sm-6">

                                    {
                                        !user.updateLoading && user.message ? 
                                            <div className={`alert alert-${user.error ? 'danger' : 'success' }`}>
                                                {user.message}
                                                <span className="close" data-dismiss="alert" aria-label="close" title="close">×</span>
                                            </div>
                                        : null
                                    }
                                    
                                    <form 
                                        className="form-horizontal"
                                        onSubmit={this.updateProfile}>

                                        <div className="form-group">
                                            <label className="col-sm-4 control-label">Full Name</label>
                                            <div className="col-sm-8">
                                                <input 
                                                    type="text"
                                                    name="fullname"
                                                    className="form-control"
                                                    defaultValue={fullname}
                                                    onChange={this.onChange}
                                                    autoComplete="off"
                                                    disabled
                                                    required />
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-4 control-label">Email Address</label>
                                            <div className="col-sm-8">
                                                <input
                                                    type="email"
                                                    className="form-control"
                                                    defaultValue={email}
                                                    autoComplete="off"
                                                    disabled />
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-4 control-label">Phone No.</label>
                                            <div className="col-sm-8">

                                            <PhoneInput
                                                className="form-control"
                                                placeholder="0700123456"
                                                defaultCountry={locale}
                                                value={phone}
                                                onChange={this.onChangePhone} />

                                                {
                                                    wallet.currency !== 'KES' ? 
                                                        null
                                                    : 
                                                    <div>
                                                        <font className="text-info">
                                                            <b>
                                                                This is the phone number you should use to pay with
                                                            </b>
                                                        </font>
                                                    </div>
                                                }
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <label className="col-sm-4 control-label">Birthday</label>
                                            <div className="col-sm-8">
                                                <DatePicker
                                                    onDatePicked={this.onDatePicked}
                                                    value={birthday} />
                                            </div>
                                        </div>

                                        <div className="form-group">
                                            <div className="col-sm-offset-4 col-sm-8">
                                                <hr />
                                                <button className="btn btn-block btn-warning">Update Details</button>
                                            </div>
                                        </div>  
                                    </form>

                                </div>

                                <div className="col-sm-offset-1 col-sm-5">
                                    <p style={{marginBottom:'0px'}}>Account Balance:</p>
                                    <h3 style={{ marginTop: '5px' }}>{`${wallet.currency} ${wallet.balance}`}</h3>
                                    <hr/>

                                    {
                                        phone && phone !== 'null' ?

                                            <div className="clearfix">
                                                <p>
                                                    Top up your MyMoviesAfrica&trade; account with any amount below:
                                                </p>

                                                <PaymentsModal
                                                    user={this.state.user}
                                                    onCloseModal={() => { this.loadUserData() }} 
                                                />

                                                {/* <p>
                                                    <font color="#5AC4CB">
                                                        <b>
                                                            Kindly ensure that the Phone Number recorded in My Account, is the Same one that You will Use to make Payment, if using Mobile Money.
                                                        </b>
                                                    </font>
                                                </p> */}
                                            </div>
                                        :
                                            <div className="col-sm-12 alert alert-danger">
                                                <span style={{ display: 'inline-block', paddingTop: '0.3em' }}> 
                                                    Please update your phone number to enable payments
                                                </span>
                                            </div>
                                    }
                                </div>
                            </div>

                            <div id="history" className="tab-pane">
                            {
                                bundle.views && bundle.views.length > 0 ?
                                    
                                    <div className="row">
                                        {
                                            bundle.views.map((view, i)=>{
                                                const ct = {
                                                    ref: view.ref,
                                                    title: view.title 
                                                }
                                                
                                                return(
                                                    <ContentCard
                                                        className="col-xs-6 col-sm-3"
                                                        contentInfo={ct}
                                                        key={i} />
                                                )
                                            })
                                        }
                                    </div>
                                :
                                    <div className="blank-state">
                                        No views
                                    </div>
                            }
                            </div>

                            <div id="favourites" className="tab-pane">
                            {
                                bundle.favourites && bundle.favourites.length > 0 ?
                                    
                                    <div className="row">
                                        {
                                            bundle.favourites.map((fav, i) => {
                                                const ct = {
                                                    ref: fav.ref,
                                                    title: fav.title
                                                }

                                                return (
                                                    <ContentCard
                                                        className="col-xs-6 col-sm-3"
                                                        contentInfo={ct}
                                                        key={i} />
                                                )
                                            })
                                        }
                                    </div>
                                :
                                    <div className="blank-state">
                                        No favourites
                                    </div>
                            }
                            </div>

                            <div id="downloads" className="tab-pane">
                                {
                                    user.downloads && user.downloads.length > 0 ?
                                        <span>All Downloads</span>
                                    :
                                    <div className="blank-state">
                                        No downloads
                                    </div>
                                }
                            </div>

                            <div id="purchases" className="tab-pane">
                                {
                                    bundle.purchases && bundle.purchases.length > 0 ?

                                    <div className="row">
                                        {
                                            bundle.purchases.map((p, i) => {
                                                const ct = {
                                                    ref: p.ref,
                                                    title: p.title,
                                                    purchase_type: p.purchase_type
                                                }

                                                return (
                                                    <ContentCard
                                                        className="col-xs-6 col-sm-3"
                                                        contentInfo={ct}
                                                        key={i} />
                                                )
                                            })
                                        }
                                    </div>
                                    :
                                    <div className="blank-state">You haven't made any purchases</div>
                                }
                            </div>

                            <div id="check-in" className="tab-pane">

                                <form 
                                    method="post" 
                                    className="form-horizontal check-in"
                                    onSubmit={this.processCheckIn}>
                                    
                                    <div className="form-group">
                                        <p>
                                            The check-in feature allows you to attend a screening on-site via MyMoviesAfrica™
                                        </p>

                                        {
                                            checkIn.message ?
                                                <div className={`alert alert-${checkIn.error ? "danger" : "success"}` }>
                                                    { checkIn.message }
                                                    <span className="close" data-dismiss="alert" aria-label="close" title="close">×</span>
                                                </div>
                                            : null
                                        }

                                        <input
                                            type="text"
                                            className="form-control pin-input"
                                            maxLength="6"
                                            placeholder="&#42;&#42;&#42;&#42;&#42;&#42;"
                                            ref="refCode"
                                            required />
                                    </div>
                            
                                    <div className="form-group">
                                        <button type="submit" className="btn btn-lg btn-block btn-danger">Check In</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </Page>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user,
        payments: state.payments
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        updateUser,
        fetchWallet,
        toggleUserFavourites, 
        fetchUserPayments,
        fetchBundle
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(MyAccount)