import React, { Component } from 'react'

import Page from '../../components/Page'
import Loader from '../../components/Loader'

import { auth } from '../../utils/firebaseAuth'


export default class Login extends Component{
    
    constructor(props){
        super(props)

        this.state = {
            email: '',
            message: '',
            loading: false
        }
    }

    onChange = (e) => {
        e.preventDefault()

        const { name, value } = e.target

        this.setState({
            [name]: value
        })
    }

    onSubmit = (e) => {
        e.preventDefault()
        this.setState({ loading:true })

        const {email} = this.state

        auth.sendPasswordResetEmail(email)
            .then(() => {
                this.setState({
                    loading: false,
                    message: 'Sent! Check your email for reset password instructions'
                })
            })
            .catch((error) => {
                this.setState({
                    loading: false,
                    message: 'Could not send reset password instructions'
                })
            })        
    }
    
    render(){
        const { email, loading, message } = this.state
        
        return(
            <Page pageTitle="Reset Password">

                <div className="clearfix login-holder">
                    <h4>Enter your email to receive reset instructions</h4>

                    {
                        loading ?
                            <Loader message="" />
                            :
                            <div>
                                {
                                    message !== '' ?
                                        <div className="alert alert-warning">{message}</div>
                                    : null
                                }
                            </div>
                    }

                    <form 
                        className="form"
                        onSubmit={this.onSubmit}>

                        <div className="form-group">
                            <input 
                                type="email" 
                                name="email"
                                defaultValue={email}
                                className="form-control"
                                placeholder="Email address"
                                onChange={this.onChange}
                                required />
                        </div>

                        <div className="form-group">
                            <button className="btn btn-block btn-success">Reset Password</button>
                        </div>
                    </form>
                </div>
            </Page>
        )
    }
}