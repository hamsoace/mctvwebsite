import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

class UpdateProfile extends Component {
    
    constructor(props){
        super(props)

        this.state = {
            phoneNumber: '',
            form: {}
        }
    }

    onChange = (e) => {
        const { name, value } = e.target

        this.setState({
            ...this.state.form,
            [name]: value
        })
    }

    onSubmit = (e) => {
        e.preventDefault()

        // update details
        console.log(this.state.form)
    }
    
    render() {
        return (
            <div className="update-profile-form clearfix">
                <form
                    className="form-horizontal"
                    onSubmit={ this.onSubmit }>
                    
                    <div className="form-group">
                        <input 
                            type="text" 
                            className="form-control"
                            name="phone"
                            onChange={ this.onChange } />
                    </div>

                    <div className="form-group">
                        <input 
                            type="date" 
                            className="form-control"
                            name="birthday"
                            onChange={ this.onChange } />
                    </div>

                    <button className="btn btn-warning">
                        Update
                    </button>
                </form>
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateProfile)