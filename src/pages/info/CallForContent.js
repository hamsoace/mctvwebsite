import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'
import Page from '../../components/Page';

class SubmitContent extends Component{

	render(){

		return(
			<Page pageTitle="Call for Content">
				<h1>Call for Content</h1>
				<div className="clearfix">
                    <p>MyMoviesAfrica&trade; accepts Audio-Visual Work that is:</p>

					<ol className="infolist">
						<li>A minimum of High Definition (720p-1080p).</li>
						<li>In MP4 Format, delivered via External Drive (i.e. No DVDs will be Accepted) 
							or FTP Site (e.g. www.wetransfer.com).</li>
						<li>An excellent Quality of Video (i.e. Not Grainy, Not Stretched, an Eye-pleasing 
							Colour Spectra, Steady Camerawork, No Jumpcuts, No Media Offline Slots, etc).</li>
						<li>An excellent Quality of Sound (i.e. Audible, Not Peaking, No Echoes, 
							Synchronised with Video, etc).</li>
						<li>Cleared for Music Rights in the Score, with Evidence of the Same.</li>
						<li>Accurate, Legible and Synchronised, if Subtitled.</li>
						<li>Accurate, Audible and Synchronised, if Dubbed into another Language.</li>
						<li>Delivered & Named in the correct Sequence and Completeness of Episodes or Parts, 
							in the Case of Series, Television Programmes, etc.</li>
						<li>Classified by the Kenya Film Classification Board (KFCB) or similar Organisation 
							in your Country of Residence (i.e. E-mail a Scan of the Certificate).</li>
						<li>Registered with the Kenya Copyright Board (KECOBO) or similar Organisation in your 
							Country of Residence (i.e. E-mail a Scan of the Certificate).</li>
						<li>With all the Metadata (e.g. as per the Credits).</li>
						<li>With various Posters, preferrably portrait</li>
						<li>Subtitles should be in WEBVTT or SRT format</li>
						<li>With quality Trailers, as High Definition MP4, as per the Audio and Visual specifications 
							(as per 1-7 above), delivered Digitally.</li>
						<li>With a Synopsis, delivered Digitally, highlighting the Plot, Cast, Director and any 
							Awards that have been Nominated or Won.</li>
						<li>With other ongoing Materials for Marketing, Promotion and Public Relations, 
							delivered Digitally.</li>
					</ol>

                    <p>
                        You can send your info to: <a href="mailto:info@mymovies.africa">info@mymovies.africa</a> or <NavLink to="/call-for-content-form" className="">click here</NavLink> to fill in a form with the necessary details.
                    </p>
				</div>
			</Page>
		);
	}
}

export default SubmitContent;