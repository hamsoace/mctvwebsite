import React, {Component} from 'react';
import Page from '../../components/Page';

import {addContentForm} from "../../utils/api";
import Swal from "sweetalert2";

class CallForContentForm extends Component {
    constructor(props) {
        super(props)

        this.state = {
            movieTitle: '',
            synopsisForm: '',
            musicRightsClearance: '',
            classificationCertificate: '',
            registrationCertificate: '',
            screener: ''
        }
    }

    handleMovieTitleChange = (event) => {
        this.setState({
            movieTitle: event.target.value,
        })
    }
    handleSynopsisFormChange = (event) => {
        this.setState({
            synopsisForm: event.target.value,

        })
    }
    handleMusicRightsClearanceChange = (event) => {
        this.setState({
            musicRightsClearance: event.target.value,
        })

    }

    handleClassificationCertificateChange = (event) => {
        this.setState({
            classificationCertificate: event.target.value,

        })
    }
    handleRegistrationCertificateChange = (event) => {
        this.setState({
            registrationCertificate: event.target.value,

        })
    }

    handleScreenerChange = (event) => {
        this.setState({
            screener: event.target.value

        })
    }
    onContentFormSubmit = event => {
        event.preventDefault();

        const obj = {
            movieTitle: this.state.movieTitle,
            synopsisForm: this.state.synopsisForm,
            musicRightsClearance: this.state.musicRightsClearance,
            classificationCertificate: this.state.classificationCertificate,
            registrationCertificate: this.state.registrationCertificate,
            screener: this.state.screener,
        }
        console.log(obj)
        addContentForm(obj)
            .then(res => {

                Swal.fire({
                    title: 'Success',
                    text: `Your request has been received!`,
                    icon: 'success',
                    confirmButtonText: 'Close'
                })
                this.setState({
                    movieTitle: "",
                    synopsisForm: "",
                    musicRightsClearance: "",
                    classificationCertificate: "",
                    registrationCertificate: "",
                    screener: ""
                });
                // this.setState({});
                // this.setState({});
                // this.setState({});
                // this.setState({registrationCertificate: ""});
                // this.setState({screener: ""});
                console.log(res);
                console.log(res.data);
            }).catch(err => {
            console.log(err)
        });
        //
        // alert(`Form submitted successfully!`)
    }


    render() {

        const {movieTitle, synopsisForm, musicRightsClearance, classificationCertificate, registrationCertificate, screener} = this.state

        return (

            <Page pageTitle="Call for Content Form">
                <h1 className="text-center">Call for Content Form</h1><br></br>
                <div className="clearfix">


                    <form onSubmit={this.onContentFormSubmit} className="form-horizontal">


                        <div className="form-group">
                            <label className="col-md-3">Movie Title</label>
                            <div className="col-md-9">
                                <input
                                    type="text"
                                    value={movieTitle}
                                    placeholder=" e.g. Kati Kati"
                                    name="movie title"
                                    className="form-control"
                                    onChange={this.handleMovieTitleChange}
                                    required/>
                            </div>
                        </div>
                        <br></br>

                        <div className="form-group">
                            <label className="col-md-3">A Synopsis highlighting the Plot, Cast, Director and any
                                Awards that have been Nominated or Won</label>
                            <div className="col-md-9">
                                <textarea
                                    type="text"
                                    value={synopsisForm}

                                    placeholder=" e.g. A young woman, Kaleche (Nyokabi Gethaiga), with no memory of her life or death, is helped with assimilation to the afterlife by a ghost called Thoma (Elsaphan Njora)...
                                 "
                                    name="synopsis"
                                    className="form-control"
                                    onChange={this.handleSynopsisFormChange}
                                    required/>
                            </div>
                        </div>
                        <br></br>

                        <div className="form-group">
                            <label className="col-md-3">Evidence of Clearance for Music Rights in the Score (Google
                                Drive Link to document)</label>
                            <div className="col-md-9">
                                <input
                                    type="text"
                                    value={musicRightsClearance}
                                    placeholder="https://drive.google.com/drive..."


                                    name="music rights clearance"
                                    className="form-control"
                                    onChange={this.handleMusicRightsClearanceChange}
                                    required/>
                            </div>
                        </div>
                        <br></br>

                        <div className="form-group">
                            <label className="col-md-3">Certificate of Classification by the Kenya Film Classification
                                Board (KFCB) or similar Organisation in your Country of Residence (Google Drive
                                Link)</label>
                            <div className="col-md-9">
                                <input
                                    type="text"
                                    value={classificationCertificate}
                                    placeholder="https://drive.google.com/drive..."


                                    name="certificate of classification"
                                    className="form-control"
                                    onChange={this.handleClassificationCertificateChange}
                                    required/>
                            </div>
                        </div>
                        <br></br>

                        <div className="form-group">
                            <label className="col-md-3">Certificate of Registration with the Kenya Copyright Board
                                (KECOBO) or similar Organisation in your Country of Residence (Google Drive
                                Link)</label>
                            <div className="col-md-9">
                                <input
                                    type="text"
                                    value={registrationCertificate}
                                    placeholder="https://drive.google.com/drive..."


                                    name="certificate of registration"
                                    className="form-control"
                                    onChange={this.handleRegistrationCertificateChange}
                                    required/>
                            </div>
                        </div>
                        <br></br>

                        <div className="form-group">
                            <label className="col-md-3">Link to a Screener for your Movie</label>
                            <div className="col-md-9">
                                <input
                                    type="text"
                                    value={screener}
                                    placeholder="https://drive.google.com/drive..."
                                    name="screener"
                                    className="form-control"
                                    onChange={this.handleScreenerChange}
                                    required/>
                            </div>
                        </div>
                        <br></br>

                        <div className="form-group">
                            <div className="col-md-offset-3 col-md-9">
                                <button className="btn btn-block btn-warning">Submit Details</button>
                            </div>
                        </div>
                        <hr/>
                    </form>

                    <p>Please note that MyMoviesAfrica&trade; will only accept Audio-Visual Work that is:</p>

                    <ol className="infolist">

                        <li>A minimum of High Definition (720p-1080p).</li>
                        <li>In MP4 Format, delivered via External Drive (i.e. No DVDs will be Accepted)
                            or FTP Site (e.g. www.wetransfer.com).
                        </li>
                        <li>An excellent Quality of Video (i.e. Not Grainy, Not Stretched, an Eye-pleasing
                            Colour Spectra, Steady Camerawork, No Jumpcuts, No Media Offline Slots, etc).
                        </li>
                        <li>An excellent Quality of Sound (i.e. Audible, Not Peaking, No Echoes,
                            Synchronised with Video, etc).
                        </li>

                        <li>Accurate, Legible and Synchronised, if Subtitled.</li>
                        <li>Accurate, Audible and Synchronised, if Dubbed into another Language.</li>
                        <li>Delivered & Named in the correct Sequence and Completeness of Episodes or Parts,
                            in the Case of Series, Television Programmes, etc.
                        </li>

                        <li>With all the Metadata (e.g. as per the Credits).</li>
                        <li>With various Posters, preferrably portrait</li>
                        <li>Subtitles should be in WEBVTT or SRT format</li>
                        <li>With quality Trailers, as High Definition MP4, as per the Audio and Visual specifications
                            (as per 1-7 above), delivered Digitally.
                        </li>

                        <li>With other ongoing Materials for Marketing, Promotion and Public Relations,
                            delivered Digitally.
                        </li>


                    </ol>
                    <p><strong>N/B: </strong>An email with further instructions will be sent to the current logged in
                        account upon successful submission of the certificates.</p>


                </div>
            </Page>
        );
    }
}

export default CallForContentForm;