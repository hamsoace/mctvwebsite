import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import Page from '../../components/Page';

export default class Page404 extends Component {
    
    render() {

        return (
            <Page>
                <div className="clearfix text-center">
                    <h1>Oops! We couldn't find that page</h1>
                    <div style={{ margin: "1em 0" }} className="text-accent smiley-404">
                        :(
                    </div>
                    <p>
                        Maybe you could try <NavLink to="/">Our Library</NavLink>
                    </p>
                </div>
            </Page>
        );
    }
}