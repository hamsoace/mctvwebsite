import React, { Component } from 'react'
import $ from 'jquery'

import {initMookhPay} from '../../utils/api'
import { getUserDetails } from '../../utils/user-utils'

import bongaLogo from '../../images/payments/bonga-points.png'
import cardsLogo from '../../images/payments/visa-mastercard-logo.png'

export default class MookhPay extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id: this.props.match.params.id,
            user: getUserDetails(),

            // Response from init call
            response:''
        }
    }

    componentDidMount = () => {
        $('body').addClass('mookh-bd')
    }

    makePaymentRequest = () => {
        this.setState({ response: '<div>Loading...</div>' })

        const { id, user } = this.state
        const { fullname, email, phone } = user

        /* Make do an order to mookh, return response */
        const paymentObj = {
            id,
            payment_method: id === '1' ? 'bonga' : 'cards',
            currency: id === '1' ? 'KES' : 'USD',
            amount: id === '1' ? 5 : 1,
            customer_name: fullname,
            customer_phone: phone,
            customer_email: email
        }

        initMookhPay(paymentObj)
            .then((response) => {
                // console.log(response.data)

                this.setState({ response: response.data })
            })
    }

    render() {
        // console.log(this.state)
        const {id, response} = this.state

        const logo = id === '1' ? bongaLogo : cardsLogo

        return (
            <div className="container" id="mookh-pay">
                <div className="clearfix pay-buttons">
                    <img
                        alt="MookhPay"
                        className="img-responsive"
                        src={logo} />

                    <button
                        className="btn btn-primary"
                        onClick={() => {
                            this.makePaymentRequest(id)
                        }}>

                        <i className="icon ion-md-card"></i> Click to Pay
                    </button>
                </div>

                <div 
                    className="clearfix" 
                    id="pay-response"
                    dangerouslySetInnerHTML={{ __html: response }}></div>
            </div>
        )
    }
}