import * as Actions from '../actions/content';
import * as UserActions from '../actions/user';

import initialState from './initialState';

export default (state = initialState.content, action) => {

    const {
        payload,
        type,
        filter = {}
    } = action;

    switch (type) {

        /**
         * 
         * Fetch Content List
         * 
         * 
        */
        case Actions.FETCH_CONTENT:
        case Actions.FETCH_CONTENT_PENDING:
            return { ...state, contentListLoading: true }

        case Actions.FETCH_CONTENT_REJECTED:
            return {
                ...state, 
                contentListLoading: false, 
                contentListError: true, 
                errorContentList: payload.data
            }

        case Actions.FETCH_CONTENT_FULFILLED:
            return {
                ...state, 
                contentListLoading: false,
                contentListError: false,  
                contentList: payload.data
            }

        /**
         * 
         * Fetch Content Info
         * 
        */
        case Actions.FETCH_CONTENT_INFO:
        case Actions.FETCH_CONTENT_INFO_PENDING:
            return { ...state, contentInfoLoading: true }

        case Actions.FETCH_CONTENT_INFO_REJECTED:
            return { ...state, contentInfoLoading: false, errorContentInfo: payload.data }

        case Actions.FETCH_CONTENT_INFO_FULFILLED:
            return { ...state, contentInfoLoading: false, contentInfo: payload.data || {} }

        
        /**
         * 
         * Set active content
         * 
        */
        case Actions.SET_ACTIVE_CONTENT:
            return { ...state, contentInfo: payload }

        
        /**
         * 
         * Search & Filter Content
         * 
        */
        case Actions.SEARCH_CONTENT:
        case Actions.SEARCH_CONTENT_PENDING:
            const searchResults = { filter, results: [] }
            return { ...state, searchLoading: true, searchResults }

        case Actions.SEARCH_CONTENT_REJECTED:
            return { ...state, searchLoading: false, errorContentSearch: payload.data }

        case Actions.SEARCH_CONTENT_FULFILLED:
            return {
                ...state,
                searchLoading: false,
                searchResults: {
                    filter,
                    results: payload.data
                }
            }

        /**
         * 
         * Fetch Recommendations
         * 
         * 
        */
        case Actions.FETCH_RECOMMENDATIONS:
        case Actions.FETCH_RECOMMENDATIONS_PENDING:
            return { ...state, recommendationsLoading: true, recommendations: [] }

        case Actions.FETCH_RECOMMENDATIONS_REJECTED:
            return { ...state, recommendationsLoading: false, errorRecommendations: payload.data }

        case Actions.FETCH_RECOMMENDATIONS_FULFILLED:

            return { ...state, recommendationsLoading: false, recommendations: payload.data ? payload.data : [] }


        /**
         * 
         * Favourites
         * 
         */
        case UserActions.TOGGLE_FAVOURITES:
        case UserActions.TOGGLE_FAVOURITES_PENDING:
            return { ...state }

        case UserActions.TOGGLE_FAVOURITES_FULFILLED:
            let { contentInfo } = state

            contentInfo = {
                ...contentInfo, 
                fav: payload.data.fav !== 'undefined' ? payload.data.fav : false
            }

            return { ...state, contentInfo }

        case UserActions.TOGGLE_FAVOURITES_REJECTED:
            contentInfo.fav = false
            return { ...state, contentInfo }

        default:
            return state;
    }
}