export default {

    content: {
        contentList: [],
        genres: [],
        contentInfo: {},
        searchResults: {
            filter: {},
            results: []
        },
        recommendations: []
    },
    user: {
        accountInfo: {},
        downloads: [],
        favourites: [],
        views: [],
        bundle: {
            favourites: [],
            views: [],
            purchases: []
        },
        wallet: {
            balance: 0,
            currency: 'KES'
        }
    },
    payments: {
        payments: [],
        purchases: []
    }
}