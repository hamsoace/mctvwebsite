import * as Actions from '../actions/payments';
import initialState from './initialState';

export default (state = initialState.payments, action) => {

    const {payload, type} = action;

    switch (type) {

        /* Payments */
        case Actions.FETCH_PAYMENTS:
        case Actions.FETCH_PAYMENTS_PENDING:
            return {...state, paymentsLoading: true, payments: []}

        case Actions.FETCH_PAYMENTS_REJECTED:
            return {...state, paymentsLoading: false, errorPayments: payload.data}

        case Actions.FETCH_PAYMENTS_FULFILLED:
            return {...state, paymentsLoading: false, payments: payload.data}


        /* Purchases */
        case Actions.FETCH_PURCHASES:
        case Actions.FETCH_PURCHASES_PENDING:
            return {...state, purchasesLoading: true, purchases: []}

        case Actions.FETCH_PURCHASES_REJECTED:
            return {...state, purchasesLoading: false, errorPurchases: payload.data}

        case Actions.FETCH_PURCHASES_FULFILLED:
            return {...state, purchasesLoading: false, purchases: payload.data}


        /* Account Topup */
        case Actions.TOPUP_ACCOUNT:
        case Actions.TOPUP_ACCOUNT_PENDING:
            return {...state, topupLoading: true}

        case Actions.TOPUP_ACCOUNT_REJECTED:
            return {...state, topupLoading: false, errorTopup: payload.data}

        case Actions.TOPUP_ACCOUNT_FULFILLED:
            return {...state, topupLoading: false, successTopup: payload.data}


        /* Buy Content */
        case Actions.BUY_CONTENT:
        case Actions.BUY_CONTENT_PENDING:
            return {...state, buyContentLoading: true, buy: {}}

        case Actions.BUY_CONTENT_REJECTED:
            return {...state, buyContentLoading: false, errorBuyContent: payload.data}

        case Actions.BUY_CONTENT_FULFILLED:
            if (action.payload.data?.purchase){
                return {...state, buyContentLoading: false, buy: payload.data,purchases: [...state.purchases,action.payload.data.purchase]}
            }
            return {...state, buyContentLoading: false, buy: payload.data}


        default:
            return state;
    }
}