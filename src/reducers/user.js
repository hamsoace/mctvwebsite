import * as Actions from '../actions/user';
import initialState from './initialState';

export default (state=initialState.user, action) => {

    const { payload, type } = action;

    switch (type){

        /**
         * 
         * Login & Auto-register
         * 
        */
        case Actions.USER_LOGIN:
        case Actions.USER_LOGIN_PENDING:
            return { ...state.user, loginLoading: true, accountInfo: {} }
        
        case Actions.USER_LOGIN_REJECTED:
            return { ...state, loginLoading: false, accountInfo: {} }

        case Actions.USER_LOGIN_FULFILLED:
            return { ...state, loginLoading: false, accountInfo: payload.data.user }

        /**
         * 
         * Update user details
         * 
         * 
        */
        case Actions.UPDATE_USER:
        case Actions.UPDATE_USER_PENDING:
            return { ...state, updateLoading: true, accountInfo: {} }

        case Actions.UPDATE_USER_REJECTED:
            return { ...state, updateLoading: false, accountInfo: {} }

        case Actions.UPDATE_USER_FULFILLED:
            const { error, message, user } = payload.data

            if(user){
                return { ...state, updateLoading: false, message, error, accountInfo: user }
            }
            else{
                return { ...state, updateLoading: false, message, error }
            }
        
        
        /**
         * 
         * Wallet
         * 
         */
        case Actions.FETCH_WALLET:
        case Actions.FETCH_WALLET_PENDING:
            return { ...state, walletLoading: true, wallet: {} }
        
        case Actions.FETCH_WALLET_REJECTED:
            return { ...state, walletLoading: false, wallet: {} }

        case Actions.FETCH_WALLET_FULFILLED:
            return { ...state, walletLoading: false, wallet: payload.data }

        
        /**
         * 
         * User bundle
         * 
         */
        case Actions.FETCH_BUNDLE:
        case Actions.FETCH_BUNDLE_PENDING:
            return { ...state, bundleLoading: true }

        case Actions.FETCH_BUNDLE_REJECTED:
            return { ...state, bundleLoading: false }

        case Actions.FETCH_BUNDLE_FULFILLED:
            return { ...state, bundleLoading: false, bundle: payload.data }

        /**
         *
         * Favourites
         *
         */


        default:
            return state;
    }
}