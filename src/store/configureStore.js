import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import promise from 'redux-promise-middleware'
import logger from 'redux-logger'
import thunk from 'redux-thunk'

import content from '../reducers/content'
import user from '../reducers/user'
import payments from '../reducers/payments'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const enhancer = composeEnhancers(
    window.location.hostname === 'localhost' ?
        applyMiddleware(promise, thunk, logger) :
        applyMiddleware(promise, thunk)
)

const reducers = combineReducers({
    content,
    user,
    payments
})

export default () => {
    return createStore(reducers, enhancer)
}