import axios from 'axios'
import URL from './urls'

axios.defaults.baseURL = URL.baseAPI
axios.defaults.timeout = 15000

const buildFormData = (formObj) => {
    const formData = new FormData()
    const keys = Object.keys(formObj)

    for (let i in keys) {
        let varname = keys[i]
        formData.append(varname, formObj[varname])
    }

    return formData
}

/**
 *
 * Content
 *
 */
export const getContent = () => {
    return axios.get(URL.content)
}

export const getContentInfo = (ref, user_id) => {
    return axios.post(URL.content, buildFormData({ref, user_id}))
}

export const findContent = (filter) => {
    return axios.post(URL.search, buildFormData(filter))
}

export const getGenres = () => {
    return axios.get(URL.genres)
}

export const getRecommendations = (criteria) => {
    return axios.post(URL.recommendations, buildFormData(criteria))
}

export const storeView = (view) => {
    return axios.post(URL.views, buildFormData(view))
}

/**
 *
 * SCREENINGS
 *
 */
export const fetchScreenings = () => {
    return axios.get(URL.screenings)
}

export const processCheckIn = (checkIn) => {
    return axios.post(URL.checkIn, buildFormData(checkIn))
}

/**
 *
 * PAYMENTS
 *
 */
export const getUserPayments = (filter) => {
    return axios.post(URL.payments, buildFormData(filter))
}

export const getUserPurchases = (filter) => {
    return axios.post(URL.purchases, buildFormData(filter))
}

export const topupAccount = (amount, mobile) => {
    return axios.post(URL.topup, buildFormData({amount, mobile}))
}

export const buyContent = (buyObj) => {
    return axios.post(URL.buy, buildFormData(buyObj))
}

export const initMookhPay = (obj) => {
    return axios.post(URL.mookhInit, buildFormData(obj))
}

/**
 *
 * USERS
 *
 */
export const login = (user) => {
    return axios.post(URL.login, buildFormData(user))
}

export const updateProfile = (user) => {
    return axios.post(URL.updateUser, buildFormData(user))
}

export const getUserBundle = (userObj) => {
    return axios.post(URL.bundle, buildFormData(userObj))
}

export const getUserWallet = (userObj) => {
    return axios.post(URL.wallet, buildFormData(userObj))
}

export const getUserFavourites = (userObj) => {
    return axios.post(URL.favourites, buildFormData(userObj))
}

export const toggleFavourites = (favObj) => {
    return axios.post(URL.favouritesToggle, buildFormData(favObj))
}

export const getUserDownloads = (userObj) => {
    return axios.post(URL.downloads, buildFormData(userObj))
}

/**
 *
 * STREAM
 *
 */
export const requestStream = (streamObj) => {
    return axios.post(URL.streamRequest, buildFormData(streamObj))
}

export const setStreamProgress = (streamObj) => {
    return axios.post(URL.storeView, buildFormData(streamObj))
}

/**movie requests */
export const addRequest = requestObj => {
    return axios.post(URL.addRequest, buildFormData(requestObj))
}

/**call for content  */
export const addContentForm = contentFormObj => {
    return axios.post(URL.addContentForm, buildFormData (contentFormObj))
}

/**
*
* Content Rating
*
* */
export const rateContent = contentObj => {
    return axios.post(URL.rateContent, contentObj)
}
export const getContentRating = contentRef => {
    return axios.get(`${URL.getContentRating + contentRef}`)
}