import URL from '../utils/urls'

/**
 * 
 * @param {contentObject} Content to be persisted
 */
const storeContent = (contentObject) => {

}

const getArtwork = (ref) => {
    return {
        portrait: URL.mediaURL + ref + '_port.jpg',
        landscape: URL.mediaURL + ref + '_land.jpg' 
    }
}

export { storeContent, getArtwork }