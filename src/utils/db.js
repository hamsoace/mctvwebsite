const LocalDB = {},
	localStorage = window.localStorage

LocalDB.isSupported = () => {
	return typeof(Storage) !== "undefined"
}

LocalDB.insert = (key, value) => {
	if(LocalDB.isSupported){
		localStorage.setItem(key, JSON.stringify(value))
		// console.log(key, value)
	}
	else{
		console.log("LocalStorage not supported")
	}
}

LocalDB.fetch = (key) => {
	if(LocalDB.isSupported){
		return JSON.parse(localStorage.getItem(key))
	}
	else{
		console.log("LocalStorage not supported")
	}
}

LocalDB.delete = (key) => {
	if(LocalDB.isSupported){
		localStorage.removeItem(key)
	}
	else{
		console.log("LocalStorage not supported")
	}
}

LocalDB.flush = () => {
	if(LocalDB.isSupported){
		localStorage.clear()
	}
	else{
		console.log("LocalStorage not supported")
	}
}

export default LocalDB