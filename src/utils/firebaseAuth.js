/**
 * https://css-tricks.com/firebase-react-part-2-user-authentication/
 */
import * as firebase from 'firebase/app'
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyDE1wRouoq6nZ5TLuREYy7MOKeURl2Dnd8",
    authDomain: "app.mymovies.africa",
    databaseURL: "https://mychoicetv-a7f9a.firebaseio.com",
    projectId: "mychoicetv-a7f9a",
    storageBucket: "mychoicetv-a7f9a.appspot.com",
    messagingSenderId: "1006067861904"
}

firebase.initializeApp(config)

export const googleAuthProvider = new firebase.auth.GoogleAuthProvider()
export const facebookAuthProvider = new firebase.auth.FacebookAuthProvider()
export const auth = firebase.auth()

export default firebase
