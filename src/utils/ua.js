import Bowser from 'bowser'

const browser = Bowser.getParser(window.navigator.userAgent)

/**
 * 
 * Get browser details
 * 
 * https://github.com/lancedikson/bowser#browser-props-detection
 * 
*/
export const getBrowser = () => {
    return browser
}

/**
 * 
 * Returns whether the current browser is suppoerted or not
 * 
 * https://github.com/lancedikson/bowser#filtering-browsers
 * 
*/
export const isSupported = () => {
    return browser.satisfies({

        desktop: {
            chrome: '>40',
            firefox: '>40',
        },

        Android: {
            // Uncomment when iOS Chrome & Safari is supported with HLS
            // safari: '>=10',
            
            chrome: '>40',
            firefox: '>40',
        },

        // Use when PlayReady is activated
        // windows: {
        //     "internet explorer": ">10",
        // },
    })
}