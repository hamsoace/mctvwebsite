const siteURL = window.location.hostname !== 'localhost' ?
    'https://www.mymovies.africa' :
    'http://localhost:3000'

const baseURL = window.location.hostname !== 'localhost' ?
    'https://api.mymovies.africa/' :
    'http://localhost:80/'

const streamURL = 'https://cdn.mymovies.africa/'
const baseAPI = baseURL + 'api/v1/'
const mediaURL = baseURL + 'content/uploads/'

const URL = {
    siteURL,
    baseAPI,
    streamURL,
    mediaURL,

    banners: mediaURL + 'banners/',

    /* Content */
    content: 'content',
    search: 'search',
    genres: 'genres',
    recommendations: 'recommendations',
    views: 'views/store',

    /* Content Rating */
    rateContent: 'rate',
    getContentRating: 'rate/',

    /*movie requests */
    addRequest: 'add/request/',

    /*content form */
    addContentForm: 'add/contentForm/',

    /* Screenings Check-ins */
    // screenings: 'screenings',
    checkIn: 'screening/checkIn',

    /* Payments */
    payments: 'users/payments',
    purchases: 'purchases',

    topup: 'mps/checkout',
    mookhInit: 'mk/init',
    payGate: 'payment/gate',

    /* Streaming */
    streamRequest: 'content/stream/request',
    storeView: 'views/store',

    /* Users */
    login: 'users/login',
    updateUser: 'users/update',
    buy: 'users/buy',

    bundle: 'users/bundle',

    wallet: 'users/wallet',
    favourites: 'users/favourites',
    favouritesToggle: 'users/favourites/toggle',

    downloads: 'users/downloads'
}

export default URL