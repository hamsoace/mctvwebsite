import Cookie from 'js-cookie'

export const setUserSession = (user) => {
    Cookie.set('user', user)
}

export const clearUserSession = () => {
    Cookie.remove('user')
}

export const getUserDetails = () => {
    const user = Cookie.getJSON('user')
    return user || {}
}

export const isLoggedIn = () => {
    return getUserDetails().hasOwnProperty('id')
}